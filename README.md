# Test drive async network
This project intends to implement and test drive an async **`tcp_server`** and **`tcp_client`**.
It used **`boost.asio`** async libraries :)

## Requirements
* cmake (minimum version required 3.16.2)

## Integration tests
The project started by implementing [`TCP_server_client_integration_test.cpp`](integration_tests/TCP_server_client_integration_test.cpp), high level, customer point of view acceptance tests. In these tests has been supposed to there is a **`tcp_server`** and **`tcp_client`** application.\
**`tcp_server`** and **`tcp_client`** must connect to each other and transfer data. Also **tcp_server** must can accept some connections from clients simultaneously.

[`TCP_server_client_integration_test.cpp`](integration_tests/TCP_server_client_integration_test.cpp)
```c++
TEST_F(Server_client_integration_test, 
    send_data_from_server_to_one_client)
{
    //...
}

TEST_F(Server_client_integration_test, 
    send_data_from_one_client_to_server)
{
    //...
}

TEST_F(Server_client_integration_test, 
    transfer_data_between_server_and_one_client_in_both_direction)
{
    //...
}

TEST_F(Server_client_integration_test, 
    transfer_data_between_server_and_some_client_in_both_direction)
{
    //...
}
```

4 test cases has been implemented. It's a long story, to cut the long story short, in each test scenario first it has been supposed there is a **`tcp_server`** and **`tcp_client`** application, then a random generated data has been send by **`tcp_server`** or **`tcp_client`**.\
**`tcp_server`** and **`tcp_client`** processes has been run through boost.process, then test runner waited for **`tcp_server`** and **`tcp_client`** to exit and finally the retrieved data by **`tcp_server`** or **`tcp_client`** has been compared with random generated data.

## **`tcp_server`** and **`tcp_client`** applications
Based on integration tests, **`tcp_server`** and **`tcp_client`** applications must give some command line options and then connect to each other.
The test cases of **`tcp_client`** has been implemented in [`TCP_client_tests.cpp`](unit_tests/TCP_client_tests.cpp). These tests resulted designing of the following interfaces:

* [`I_TCP_client.h`](include/network/I_TCP_client.h)
```c++
class I_TCP_client {

public:

	using On_connect_function_type = 
		std::function<void(const IErrorCode&, std::shared_ptr<I_session>)>;

	virtual ~I_TCP_client() = default;

	virtual void async_connect(
		ProtocolType protocol,
		const std::string& IP_address, 
		int port, 
		On_connect_function_type on_connect,
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) = 0;
	virtual void stop() = 0;

};
```
* [`I_socket.h`](include/network/I_socket.h)
* [`I_session.h`](include/network/I_session.h)
* [`I_IO_context.h`](include/network/I_IO_context.h)
* [`I_session_controller.h`](include/network/I_session_controller.h)
* [`I_circular_buffer.h`](include/concurrency/I_circular_buffer.h)
* [`I_error_code.h`](include/I_error_code.h)

The test cases of **`tcp_server`** has been implemented in [`TCP_server_tests.cpp`](unit_tests/TCP_server_tests.cpp). These tests resulted designing of the following interfaces:

* [`I_TCP_server.h`](include/network/I_TCP_server.h)
```c++
class I_TCP_server {

public:

	using On_accept_function_type = 
		std::function<void(const I_error_code&, std::shared_ptr<I_session>)>;

	virtual ~I_TCP_server() = default;

	virtual void start_async_accept(
		int port, 
		const On_accept_function_type& on_accept,
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) = 0;
	virtual void stop_async_accept() = 0;

};
```
* [`I_TCP_acceptor.h`](include/network/I_TCP_acceptor.h)

In the [unit_tests](unit_tests) directory there are test cases of concrete classes of these interfaces.