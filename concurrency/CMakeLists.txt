include(Util)

find_or_get(Boost)

set(cpp_files 
    error_code.cpp
    StopTask.cpp
    TaskHandler.cpp
    TaskRunner.cpp
    ThreadPool.cpp
    ThreadPoolFactory.cpp)

add_library(concurrency STATIC ${cpp_files})
target_include_directories(concurrency PUBLIC
                            ${INCLUDE_DIRS}
                            ${INCLUDE_DIRS}/concurrency
                            ${Boost_INCLUDE_DIRS})
target_precompile_headers(concurrency PUBLIC pch.h)
set_property(TARGET concurrency PROPERTY CXX_STANDARD 17)

