#include "pch.h"

#include "Stop_task.h"

void Stop_task::stop()
{
	_is_stop = true;
}

bool Stop_task::is_stop() const
{
	return _is_stop;
}
