#include "pch.h"

#include "Task_handler.h"

Task_handler::Task_handler(
	std::shared_ptr<I_stop_task> taskStopSignal, 
	std::future<bool> taskResult):
	_task_stop_signal(std::move(taskStopSignal)),
	_task_result(std::move(taskResult))
{
}

Task_handler::~Task_handler()
{
	stop();
}

void Task_handler::stop()
{
	if(!_task_is_stopped)
	{
		_task_stop_signal->stop();
		_task_is_stopped = true;
	}
}

bool Task_handler::get()
{
	return _task_result.get();
}
