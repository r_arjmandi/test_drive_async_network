#include "pch.h"

#include "Task_runner.h"
#include "Task_handler.h"
#include "Stop_task.h"

std::shared_ptr<I_task_handler> Task_runner::run_async(
	std::shared_ptr<I_task> task)
{
	auto task_stop_signal {std::make_shared<Stop_task>()};
	auto task_result {std::async(
		std::launch::async, 
		[func_obj {task}, is_task_stopped {task_stop_signal}]() mutable
		{
			return (*func_obj)(is_task_stopped);
		})};
	return std::make_shared<Task_handler>(
		task_stop_signal, std::move(task_result));
}

std::shared_ptr<I_task_handler> Task_runner::run_async(
	std::function<bool(std::shared_ptr<I_stop_token> is_task_stopped)> task)
{
	auto task_stop_signal {std::make_shared<Stop_task>()};
	auto task_result {std::async(
		std::launch::async,
		[func_obj {std::move(task)}, 
		is_task_stopped {task_stop_signal}]() mutable
		{
			return func_obj(is_task_stopped);
		})};
	return std::make_shared<Task_handler>(
		task_stop_signal, std::move(task_result));
}
