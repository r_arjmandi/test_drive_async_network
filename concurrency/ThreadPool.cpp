#include "pch.h"

#include "Thread_pool.h"

Thread_pool::Thread_pool(unsigned int number_of_threads):
	_thread_pool(number_of_threads),
	_exectuor(*this)
{
}

Thread_pool::~Thread_pool()
{
}

I_executor<I_thread_pool>& Thread_pool::get_executor()
{
	return _exectuor;
}

void Thread_pool::join()
{
	_thread_pool.join();
}

void Thread_pool::stop()
{
	_thread_pool.stop();
}

boost::asio::thread_pool& Thread_pool::get_native_context()
{
	return _thread_pool;
}

Thread_pool::Executor::Executor(Thread_pool& context):
	_context(context)
{
}

void Thread_pool::Executor::post(std::function<void()> func)
{
	boost::asio::post(_context.get_native_context(), func);
}

I_thread_pool& Thread_pool::Executor::context()
{
	return _context;
}
