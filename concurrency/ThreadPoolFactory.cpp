#include "pch.h"

#include "Thread_pool_factory.h"

#include "Thread_pool.h"

std::shared_ptr<I_thread_pool> Thread_pool_factory::create(
	unsigned int number_of_thread)
{
	return std::make_shared<Thread_pool>(number_of_thread);
}
