#include "pch.h"

#include "error_code.h"

error_code::error_code(int value, bool is_error, const std::string& message):
	_value(value),
	_is_error(is_error),
	_message(message)
{
}

std::string error_code::message() const
{
	return _message;
}

int error_code::value() const
{
	return _value;
}

error_code::operator bool() const
{
	return _is_error;
}