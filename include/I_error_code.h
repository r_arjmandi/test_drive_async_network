#pragma once

class I_error_code {
	
public:

	virtual ~I_error_code() = default;

	virtual std::string message() const = 0;
	virtual int value() const = 0;
	virtual explicit operator bool() const = 0;

};