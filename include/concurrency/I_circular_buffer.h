#pragma once

template<class T>
class I_circular_buffer {

public:

	virtual ~I_circular_buffer() = default;

	virtual void pop_back() = 0;
	virtual T& back() = 0;
	virtual void push_front(const T& value) = 0;
	virtual bool full() const = 0;
	virtual bool empty() const = 0;

};