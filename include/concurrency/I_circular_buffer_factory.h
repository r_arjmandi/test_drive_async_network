#pragma once

#include "I_circular_buffer.h"

template<class T>
class I_circular_buffer_factory {

public:

	virtual ~I_circular_buffer_factory() = default;

	virtual std::shared_ptr<I_circular_buffer<T>> create(
		unsigned long long int size) = 0;

};