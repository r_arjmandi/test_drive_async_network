#pragma once

template<class Execution_context>
class I_executor {

public:

	virtual ~I_executor() = default;

	virtual void post(std::function<void()> func) = 0;
	virtual Execution_context& context() = 0;

};