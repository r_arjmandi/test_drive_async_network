#pragma once

class I_stop_task {

public:

	virtual ~I_stop_task() = default;

	virtual void stop() = 0;

};