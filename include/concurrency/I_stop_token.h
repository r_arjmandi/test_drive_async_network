#pragma once

class I_stop_token {

public:

	virtual ~I_stop_token() = default;

	virtual bool is_stop() const = 0;

};