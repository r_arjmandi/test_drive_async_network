#pragma once

#include "I_stop_token.h"

class I_task {
	
public:

	virtual ~I_task() = default;

	virtual bool operator()(
		std::shared_ptr<I_stop_token> is_task_stopped) = 0;

};