#pragma once

class I_task_handler {
	
public:

	virtual ~I_task_handler() = default;

	virtual void stop() = 0;
	virtual bool get() = 0;

};