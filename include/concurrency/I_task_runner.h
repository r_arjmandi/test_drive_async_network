#pragma once

#include "I_task.h"
#include "I_task_handler.h"

class I_task_runner {
	
public:

	virtual ~I_task_runner() = default;

	virtual std::shared_ptr<I_task_handler> run_async(
		std::shared_ptr<I_task> task) = 0;
	virtual std::shared_ptr<I_task_handler> run_async(
		std::function<bool(
			std::shared_ptr<I_stop_token> is_task_stopped)> task) = 0;

};
