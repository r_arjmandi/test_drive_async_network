#pragma once

#include "I_executor.h"

class I_thread_pool {

public:

	virtual ~I_thread_pool() = default;

	virtual void join() = 0;
	virtual void stop() = 0;
	virtual I_executor<I_thread_pool>& get_executor() = 0;

};