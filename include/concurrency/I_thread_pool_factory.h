#pragma once

#include "I_thread_pool.h"

class I_thread_pool_factory {

public:

	virtual ~I_thread_pool_factory() = default;

	virtual std::shared_ptr<I_thread_pool> 
		create(unsigned int number_of_thread) = 0;

};