#pragma once

#include "I_stop_task.h"
#include "I_stop_token.h"

class Stop_task : public I_stop_task, public I_stop_token {

public:

	void stop() override;
	bool is_stop() const override;

private:

	std::atomic<bool> _is_stop {false};

};