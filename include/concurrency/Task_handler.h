#pragma once

#include "I_task_handler.h"
#include "I_stop_task.h"

class Task_handler : public I_task_handler {

public:

	Task_handler(
		std::shared_ptr<I_stop_task> task_stop_signal,
		std::future<bool> task_result);
	~Task_handler() override;

	void stop() override;
	bool get() override;

private:

	std::shared_ptr<I_stop_task> _task_stop_signal;
	std::future<bool> _task_result;
	bool _task_is_stopped {false};

};
