#pragma once

#include "I_task_runner.h"

class Task_runner : public I_task_runner {

public:

	std::shared_ptr<I_task_handler> run_async(
		std::shared_ptr<I_task> task) override;
	std::shared_ptr<I_task_handler> run_async(std::function<bool(
		std::shared_ptr<I_stop_token> is_task_stopped)> task) override;

};