#pragma once

#include "I_thread_pool.h"

class Thread_pool : 
	public I_thread_pool, 
	public std::enable_shared_from_this<Thread_pool> {

public:
	
	class Executor : public I_executor<I_thread_pool>
	{

	public:

		Executor(Thread_pool& context);

		void post(std::function<void()> func) override;
		I_thread_pool& context() override;

	private:

		Thread_pool& _context;
	
	};

	Thread_pool(unsigned int number_of_threads);
	~Thread_pool() override;
	I_executor<I_thread_pool>& get_executor() override;
	void join() override;
	void stop() override;

	boost::asio::thread_pool& get_native_context();

private:

	Executor _exectuor;
	boost::asio::thread_pool _thread_pool;

};