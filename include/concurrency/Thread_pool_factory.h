#pragma once

#include "I_thread_pool_factory.h"

class Thread_pool_factory : public I_thread_pool_factory {

public:
	
	std::shared_ptr<I_thread_pool> 
		create(unsigned int number_of_thread) override;

};