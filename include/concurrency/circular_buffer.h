#pragma once

#include "I_circular_buffer.h"

template<class T>
class circular_buffer : public I_circular_buffer<T> {

public:

	circular_buffer(const circular_buffer&) = delete;
	circular_buffer& operator= (const circular_buffer&) = delete;

	explicit circular_buffer(
		typename boost::circular_buffer<T>::size_type capacity) :
		_container(capacity)
	{
	}

	void push_front(const T& value) override
	{
		std::lock_guard<std::mutex> lock(_mutex);
		_container.push_front(value);
	}

	void pop_back() override
	{
		std::lock_guard<std::mutex> lock(_mutex);
		_container.pop_back();
	}

	T& back() override
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return _container.back();
	}

	bool full() const override
	{
		return _container.full();
	}

	bool empty() const override
	{
		return _container.empty();
	}

private:

	boost::circular_buffer<T> _container;
	std::mutex _mutex;

};
