#pragma once

#include "I_circular_buffer_factory.h"
#include "circular_buffer.h"

template<class T>
class circular_buffer_factory : public I_circular_buffer_factory<T> {

public:
	
	std::shared_ptr<I_circular_buffer<T>> create(
		unsigned long long int size) override
	{
		return std::make_shared<circular_buffer<T>>(size);
	}

};
