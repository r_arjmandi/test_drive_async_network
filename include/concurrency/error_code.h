#pragma once

#include "I_error_code.h"

class error_code : public I_error_code {

public:
	
	error_code(int value, bool is_error, const std::string& message);
	virtual std::string message() const override;
	virtual int value() const override;
	virtual operator bool() const override;

private:

	int _value;
	bool _is_error; 
	std::string _message;

};