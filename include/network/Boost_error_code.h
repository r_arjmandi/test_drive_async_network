#pragma once

#include "I_error_code.h"

class Boost_error_code : public I_error_code {

public:

	Boost_error_code(const boost::system::error_code& ec);
	
	std::string message() const override;
	int value() const override;
	explicit operator bool() const override;

private:

	boost::system::error_code _ec;

};