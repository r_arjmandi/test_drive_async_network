#pragma once

#include "I_IO_context.h"

#include "concurrency/I_executor.h"

class IO_context : 
	public I_IO_context, public std::enable_shared_from_this<IO_context> {

public:

	class IO_executor : public I_executor<I_IO_context> {

	public:

		IO_executor(IO_context& ioContext);

		void post(std::function<void()> func) override;
		I_IO_context& context() override;
	
	private:

		IO_context& _context;

	};

	IO_context();

	int run() override;
	int run_one() override;
	int run_for(const std::chrono::milliseconds& miliSeconds) override;
	void stop() override;
	void restart() override;

	I_executor<I_IO_context>& get_executor() override;

	boost::asio::io_context& get_native_IO_context();

private:

	boost::asio::io_context _IO_context;
	IO_executor _executor;

};