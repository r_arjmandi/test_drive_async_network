#pragma once

#include "I_IO_context_factory.h"

class IO_context_factory : public I_IO_context_factory {
	
public:

	std::shared_ptr<I_IO_context> create() override;

};