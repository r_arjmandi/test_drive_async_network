#pragma once

#include "concurrency/I_executor.h"

class I_IO_context {
	
public:

	virtual ~I_IO_context() = default;

	virtual int run() = 0;
	virtual int run_one() = 0;
	virtual int run_for(const std::chrono::milliseconds& miliSeconds) = 0;
	virtual void stop()  = 0;
	virtual void restart() = 0;
	virtual I_executor<I_IO_context>& get_executor() = 0;

};