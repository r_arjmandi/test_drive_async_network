#pragma once

#include "I_IO_context.h"

class I_IO_context_factory {

public:

	virtual ~I_IO_context_factory() = default;

	virtual std::shared_ptr<I_IO_context> create() = 0;

};