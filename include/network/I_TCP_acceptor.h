#pragma once

#include "I_socket.h"
#include "I_error_code.h"

class I_TCP_acceptor {

public:

	using On_accept_function_type = std::function<void(const I_error_code&)>;

	virtual ~I_TCP_acceptor() = default;

	virtual void async_accept(
		std::shared_ptr<I_socket> socket,
		const On_accept_function_type& onAccept) = 0;
	virtual void close() = 0;
	virtual void cancel() = 0;
	virtual void release() = 0;

};