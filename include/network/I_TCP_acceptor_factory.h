#pragma once

#include "I_TCP_acceptor.h"
#include "I_IO_context.h"

class I_TCP_acceptor_factory {
	
public:

	virtual ~I_TCP_acceptor_factory() = default;

	virtual std::shared_ptr<I_TCP_acceptor> create(
		std::shared_ptr<I_IO_context> IO_context_, int port) = 0;

};