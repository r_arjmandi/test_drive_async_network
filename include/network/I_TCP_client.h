#pragma once

#include "I_error_code.h"
#include "I_session.h"

class I_TCP_client {

public:

	using On_connect_function_type = 
		std::function<void(const I_error_code&, std::shared_ptr<I_session>)>;

	virtual ~I_TCP_client() = default;

	virtual void async_connect(
		const std::string& IP_address, 
		int port, 
		On_connect_function_type on_connect,
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) = 0;
	virtual void stop() = 0;

};