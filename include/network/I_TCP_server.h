#pragma once

#include "I_session.h"
#include "I_error_code.h"

class I_TCP_server {

public:

	using On_accept_function_type = 
		std::function<void(const I_error_code&, std::shared_ptr<I_session>)>;

	virtual ~I_TCP_server() = default;

	virtual void start_async_accept(
		int port, 
		const On_accept_function_type& on_accept,
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) = 0;
	virtual void stop_async_accept() = 0;

};