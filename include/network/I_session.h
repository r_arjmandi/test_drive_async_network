#pragma once

#include "concurrency/I_circular_buffer.h"

#include "Packet.h"
#include "I_socket.h"

class I_session {

public:

	virtual ~I_session() = default;

	virtual void start(
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) = 0;
	virtual void stop() = 0;
	virtual std::shared_ptr<I_circular_buffer<Packet>> 
		get_send_buffer() const = 0;
	virtual std::shared_ptr<I_circular_buffer<Packet>> 
		get_receive_buffer() const = 0;

};