#pragma once

#include "I_session.h"
#include "I_IO_context.h"

class I_session_controller {

public:

	virtual ~I_session_controller() = default;

	virtual void start(
		std::shared_ptr<I_session> session,
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) = 0;
	virtual void stop(std::shared_ptr<I_session> session) = 0;
	virtual void stop_all() = 0;

};