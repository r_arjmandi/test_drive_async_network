#pragma once

#include "I_session_controller.h"

class I_session_controller_factory {

public:

	virtual ~I_session_controller_factory() = default;

	virtual std::shared_ptr<I_session_controller> create() = 0;

};