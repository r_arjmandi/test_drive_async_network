#pragma once

#include "I_session.h"

#include "I_socket.h"
#include "I_session_controller.h"

#include "concurrency/I_circular_buffer.h"

class I_session_factory {
	
public:

	virtual ~I_session_factory() = default;

	virtual std::shared_ptr<I_session> create(
		std::shared_ptr<I_session_controller> session_controller,
		std::shared_ptr<I_socket> socket,
		std::shared_ptr<I_circular_buffer<Packet>> receive_buffer,
		std::shared_ptr<I_circular_buffer<Packet>> send_buffer) = 0;

};
