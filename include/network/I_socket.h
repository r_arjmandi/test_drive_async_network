#pragma once

#include "I_error_code.h"
#include "Packet.h"
#include "I_IO_context.h"

#include "concurrency/I_executor.h"

class I_socket {

public:

	using On_connect_function_type = std::function<void(const I_error_code&)>;
	using On_async_write_function_type = 
		std::function<void(const I_error_code&)>;
	using On_async_read_function_type = 
		std::function<void(const I_error_code&)>;

	virtual ~I_socket() = default;

	virtual void close() = 0;
	virtual void shutdown() = 0;
	virtual void async_connect(
		const std::string& IP_address, 
		int port, 
		const On_connect_function_type& on_connect) = 0;
	virtual void async_write(
		const Packet& packet,
		const On_async_write_function_type& on_async_write) = 0;
	virtual void async_read(
		Packet& packet,
		const On_async_read_function_type& on_async_read) = 0;
	virtual I_executor<I_IO_context>& get_executor() = 0;

};