#pragma once

#include "I_socket.h"

class I_socket_factory {

public:

	virtual ~I_socket_factory() = default;
	virtual std::shared_ptr<I_socket> create(
		std::shared_ptr<I_IO_context> IO_context_) = 0;

};