#pragma once
#include "I_session.h"

#include "concurrency/I_circular_buffer.h"

#include "I_session_controller.h"
#include "Packet.h"
#include "I_IO_context.h"
#include "I_socket.h"

class Session : 
	public I_session, public std::enable_shared_from_this<Session> {

public:

	Session(
		std::shared_ptr<I_session_controller> session_controller,
		std::shared_ptr<I_socket> socket,
		std::shared_ptr<I_circular_buffer<Packet>> receive_buffer,
		std::shared_ptr<I_circular_buffer<Packet>> send_buffer);;

	~Session() override;

	void start(
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) override;
	void stop() override;
	std::shared_ptr<I_circular_buffer<Packet>> get_send_buffer() const override;
	std::shared_ptr<I_circular_buffer<Packet>> 
		get_receive_buffer() const override;

private:

	void start_async_read();
	void try_push_to_receive_buffer();
	void start_async_write();

	std::shared_ptr<I_session_controller> _session_controller;
	std::shared_ptr<I_socket> _socket;
	std::shared_ptr<I_circular_buffer<Packet>> _receive_buffer;
	std::shared_ptr<I_circular_buffer<Packet>> _send_buffer;
	Packet _read_packet;
	Packet _write_packet;
	Packet _end_send_packet;
	Packet _end_receive_packet;
	bool _is_session_stopped = true;

};
