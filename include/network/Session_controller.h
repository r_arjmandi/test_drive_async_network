#pragma once

#include "I_session_controller.h"

class Session_controller : public I_session_controller {

public:

	~Session_controller() override;

	void start(
		std::shared_ptr<I_session> session,
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) override;
	void stop(std::shared_ptr<I_session> session) override;
	void stop_all() override;

private:

	std::set<std::shared_ptr<I_session>> _sessions;

};