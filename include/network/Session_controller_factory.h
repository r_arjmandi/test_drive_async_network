#pragma once

#include "I_session_controller_factory.h"

class Session_controller_factory : public I_session_controller_factory {

public:

	std::shared_ptr<I_session_controller> create() override;

};