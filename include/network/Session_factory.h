#pragma once

#include "I_session_factory.h"

#include "Packet.h"
#include "I_IO_context.h"
#include "Session.h"

class Session_factory : public I_session_factory {

public:

	std::shared_ptr<I_session> create(
		std::shared_ptr<I_session_controller> session_controller,
		std::shared_ptr<I_socket> socket,
		std::shared_ptr<I_circular_buffer<Packet>> receive_buffer,
		std::shared_ptr<I_circular_buffer<Packet>> send_buffer) override;

};
