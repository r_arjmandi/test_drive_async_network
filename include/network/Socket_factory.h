#pragma once

#include "I_socket_factory.h"

class Socket_factory : public I_socket_factory {

public:

	std::shared_ptr<I_socket> create(
		std::shared_ptr<I_IO_context> IO_context_) override;

};