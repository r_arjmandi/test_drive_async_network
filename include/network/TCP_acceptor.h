#pragma once

#include "I_TCP_acceptor.h"

#include "I_IO_context.h"

class TCP_acceptor : public I_TCP_acceptor {

public:

	TCP_acceptor(std::shared_ptr<I_IO_context> IO_context_, int port);

	void async_accept(
		std::shared_ptr<I_socket> socket,
		const On_accept_function_type& on_accept) override;
	void close() override;
	void cancel() override;
	void release() override;

private:

	boost::asio::ip::tcp::acceptor _acceptor;
	std::shared_ptr<I_IO_context> _IO_context;

};