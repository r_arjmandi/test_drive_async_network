#pragma once

#include "I_TCP_acceptor_factory.h"

class TCP_acceptor_factory : public I_TCP_acceptor_factory {

public:

	std::shared_ptr<I_TCP_acceptor> create(
		std::shared_ptr<I_IO_context> IO_context_, int port) override;

};