#pragma once

#include "I_TCP_client.h"

#include "concurrency/I_task_runner.h"
#include "concurrency/I_circular_buffer_factory.h"
#include "concurrency/I_thread_pool_factory.h"

#include "I_session_factory.h"
#include "I_socket_factory.h"
#include "I_IO_context_factory.h"
#include "I_IO_context_factory.h"
#include "I_session_controller_factory.h"

class TCP_client : public I_TCP_client {

public:

	TCP_client(
		std::shared_ptr<I_IO_context> IO_context,
		std::shared_ptr<I_socket_factory> socket_factory,
		std::shared_ptr<I_session_factory> session_factory,
		std::shared_ptr<I_session_controller_factory> session_controller_factory,
		std::shared_ptr<I_circular_buffer_factory<Packet>> 
			circular_buffer_factory,
		unsigned long long int send_buffer_size,
		unsigned long long int receive_buffer_size);

	~TCP_client() override;

	void async_connect(
		const std::string& IP_address,
		int port,
		On_connect_function_type on_connect,
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) override;
	void stop() override;

private:

	void socket_on_connect(const I_error_code& error_code);

	std::shared_ptr<I_IO_context> _IO_context;
	std::shared_ptr<I_socket_factory> _socket_factory;
	std::shared_ptr<I_session_factory> _session_factory;
	std::shared_ptr<I_session_controller> _session_controller;
	std::shared_ptr<I_circular_buffer_factory<Packet>> _circular_buffer_factory;
	std::shared_ptr<I_socket> _socket;
	unsigned long long int _send_buffer_size;
	unsigned long long int _receive_buffer_size;
	bool _is_run {false};
	bool _is_session_started {false};
	bool _enable_receive {false}; 
	bool _enable_send {false};
	Packet _end_receive_packet; 
	Packet _end_send_packet;

	On_connect_function_type _on_connect;

};
