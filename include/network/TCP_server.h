#pragma once

#include "I_TCP_server.h"

#include "concurrency/I_task_runner.h"
#include "concurrency/I_circular_buffer_factory.h"
#include "concurrency/I_thread_pool_factory.h"

#include "I_session_factory.h"
#include "I_session_controller.h"
#include "I_session_controller_factory.h"
#include "I_IO_context_factory.h"
#include "I_socket_factory.h"
#include "I_TCP_acceptor_factory.h"

class TCP_server : public I_TCP_server {

public:

	TCP_server(
		std::shared_ptr<I_IO_context> IO_context,
		std::shared_ptr<I_socket_factory> socket_factory,
		std::shared_ptr<I_TCP_acceptor_factory> acceptor_factory,
		std::shared_ptr<I_session_factory> session_factory,
		std::shared_ptr<I_session_controller_factory> session_controller_factory,
		std::shared_ptr<I_circular_buffer_factory<Packet>> 
			circular_buffer_factory,
		unsigned long int send_buffer_size_per_client,
		unsigned long int receive_buffer_size_per_client);
	~TCP_server();

	void start_async_accept(
		int port, 
		const On_accept_function_type& on_accept, 
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet) override;
	void stop_async_accept() override;

private:

	void DoAccept();
	void on_accept(
		std::shared_ptr<I_socket> socket,
		std::shared_ptr<I_IO_context> socket_IO_context,
		const I_error_code& ec);

	std::shared_ptr<I_IO_context> _IO_context;
	std::shared_ptr<I_socket_factory> _socket_factory;
	std::shared_ptr<I_TCP_acceptor_factory> _acceptor_factory;
	std::shared_ptr<I_session_factory> _session_factory;
	std::shared_ptr<I_circular_buffer_factory<Packet>> _circular_buffer_factory;
	std::shared_ptr<I_session_controller> _session_controller;
	unsigned long int _send_buffer_size_per_client;
	unsigned long int _receive_buffer_size_per_client;
	std::shared_ptr<I_TCP_acceptor> _acceptor;
	On_accept_function_type _on_accept;
	bool _is_async_accept_started {false};
	bool _session_is_started {false};
	bool _enable_receive {false}; 
	bool _enable_send {false};
	Packet _end_receive_packet; 
	Packet _end_send_packet;

};