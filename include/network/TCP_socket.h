#pragma once

#include "I_socket.h"

#include "I_IO_context.h"

class TCP_socket : public I_socket {

public:

	TCP_socket(std::shared_ptr<I_IO_context> ioContext);
	~TCP_socket() override;

	void close() override;
	void shutdown() override;
	void async_connect(
		const std::string& IP_address, 
		int port, 
		const On_connect_function_type& on_connect) override;
	void async_write(
		const Packet& packet,
		const On_async_write_function_type& on_async_write) override;
	void async_read(
		Packet& packet, 
		const On_async_read_function_type& on_async_read) override;
	I_executor<I_IO_context>& get_executor() override;

	boost::asio::ip::tcp::socket& get_native_socket();

private:
	
	std::shared_ptr<I_IO_context> _IO_context;
	boost::asio::ip::tcp::socket _socket;

};