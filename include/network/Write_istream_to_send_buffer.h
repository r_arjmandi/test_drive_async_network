#pragma once

#include "concurrency/I_circular_buffer.h"
#include "concurrency/I_executor.h"

#include "network/Packet.h"
#include "network/I_IO_context.h"

class Write_istream_to_send_buffer : 
	public std::enable_shared_from_this<Write_istream_to_send_buffer> {

public:

	Write_istream_to_send_buffer(
		std::istream& istream_,
		I_executor<I_IO_context>& executor,
		std::shared_ptr<I_circular_buffer<Packet>> send_buffer);

	void start(const std::function<void(void)>& finish_handler);

private:

	void read_a_packet();
	void push_to_send_buffer(const Packet& packet);
	void try_run_finish_handler();

	I_executor<I_IO_context>& _executor;
	std::shared_ptr<I_circular_buffer<Packet>> _send_buffer;
	std::function<void()> _finish_handler;
	Packet _packet;
	Packet _end_packet;
	std::istream& _istream;

};