#pragma once

#include "concurrency/I_circular_buffer.h"
#include "concurrency/I_executor.h"

#include "Packet.h"
#include "I_IO_context.h"

class Write_receive_buffer_to_ostream : 
	public std::enable_shared_from_this<Write_receive_buffer_to_ostream> {

public:

	Write_receive_buffer_to_ostream(
		std::ostream& ostream,
		I_executor<I_IO_context>& executor,
		std::shared_ptr<I_circular_buffer<Packet>> receive_buffer);
	void start(const std::function<void()>& finish_handler);

private:

	void read_from_receive_buffer();

	I_executor<I_IO_context>& _executor;
	std::shared_ptr<I_circular_buffer<Packet>> _receive_buffer;
	std::function<void()> _finish_handler;
	Packet _packet;
	Packet _end_packet;
	std::ostringstream _data;
	std::ostream& _ostream;

};