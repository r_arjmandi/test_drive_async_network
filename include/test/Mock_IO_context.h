#pragma once

#include "network/I_IO_context.h"

class Mock_IO_context : public I_IO_context {

public:
	
	MOCK_METHOD0(run, int());
	MOCK_METHOD0(run_one, int());
	MOCK_METHOD1(run_for, int (const std::chrono::milliseconds& miliSeconds));
	MOCK_METHOD0(stop, void());
	MOCK_METHOD0(restart, void());
	MOCK_METHOD0(get_executor, I_executor<I_IO_context>&());

};