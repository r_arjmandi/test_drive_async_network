#pragma once

#include "network/I_IO_context_factory.h"

class Mock_IO_context_factory : public I_IO_context_factory
{

public:

	MOCK_METHOD0(create, std::shared_ptr<I_IO_context>());

};