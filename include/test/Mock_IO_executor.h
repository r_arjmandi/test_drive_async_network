#pragma once

#include "concurrency/I_executor.h"

#include "network/I_IO_context.h"

class Mock_IO_executor : public I_executor<I_IO_context> {

public:

	MOCK_METHOD1(post, void(std::function<void()> func));
	MOCK_METHOD0(context, I_IO_context&());

};