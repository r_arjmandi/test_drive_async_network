#pragma once

#include "network/I_TCP_acceptor.h"

class Mock_acceptor : public I_TCP_acceptor {

public:

	MOCK_METHOD2(async_accept,
		void(std::shared_ptr<I_socket> socket,
			const On_accept_function_type& onAccept));
	MOCK_METHOD0(close, void());
	MOCK_METHOD0(cancel, void());
	MOCK_METHOD0(release, void());

};
