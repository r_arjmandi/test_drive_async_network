#pragma once

#include "network/I_TCP_acceptor_factory.h"

class Mock_acceptor_factory : public I_TCP_acceptor_factory {

public:

	MOCK_METHOD2(
		create, std::shared_ptr<I_TCP_acceptor>(
			std::shared_ptr<I_IO_context>, int));

};