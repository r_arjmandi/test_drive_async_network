#pragma once

#include "concurrency/I_circular_buffer.h"

template<class T> 
class Mock_circular_buffer : public I_circular_buffer<T> {

public:

	MOCK_METHOD0_T(pop_back, void ());
	MOCK_METHOD0_T(back, T&());
	MOCK_METHOD1_T(push_front, void(const T& value));
	MOCK_CONST_METHOD0_T(full, bool());
	MOCK_CONST_METHOD0_T(empty, bool());

};