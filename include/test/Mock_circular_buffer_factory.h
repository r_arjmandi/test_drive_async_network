#pragma once

#include "concurrency/I_circular_buffer_factory.h"

template<class T>
class Mock_circular_buffer_factory : public I_circular_buffer_factory<T> {

public:

	MOCK_METHOD1_T(
		create, std::shared_ptr<I_circular_buffer<T>>(
			unsigned long long int size));

};