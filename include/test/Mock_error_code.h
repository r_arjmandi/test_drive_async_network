#pragma once

#include "I_error_code.h"

class Mock_error_code : public I_error_code {

public:

	MOCK_CONST_METHOD0(message, std::string());
	MOCK_CONST_METHOD0(value, int());
	MOCK_CONST_METHOD0(bool_operator, bool());

	explicit operator bool() const override
	{
		return bool_operator();
	}

};
