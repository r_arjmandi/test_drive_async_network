#pragma once

#include "network/I_session.h"
#include "I_error_code.h"

class Mock_on_accept_function_object {

public:

	virtual ~Mock_on_accept_function_object() = default;

	void operator()(
		const I_error_code& error_code, std::shared_ptr<I_session> session)
	{
		parenthese_op(error_code, session);
	}

	MOCK_METHOD2(parenthese_op, 
		void(const I_error_code& error_code, 
			std::shared_ptr<I_session> session));

};