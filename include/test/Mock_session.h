#pragma once

#include "network/I_session.h"

class Mock_session : public I_session {

public:

	MOCK_METHOD4(start, void(
		bool enable_receive, 
		bool enable_send,
		Packet& end_receive_packet, 
		Packet& end_send_packet));
	MOCK_METHOD0(stop, void());
	MOCK_CONST_METHOD0(get_send_buffer, std::shared_ptr<I_circular_buffer<Packet>>());
	MOCK_CONST_METHOD0(get_receive_buffer, std::shared_ptr<I_circular_buffer<Packet>>());

};