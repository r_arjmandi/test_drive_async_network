#pragma once

#include "network/I_session_controller.h"

class Mock_session_controller : public I_session_controller {

public:

	MOCK_METHOD5(start, void(
		std::shared_ptr<I_session> session,
		bool enable_receive, 
		bool enable_send, 
		Packet& end_receive_packet, 
		Packet& end_send_packet));
	MOCK_METHOD1(stop, void(std::shared_ptr<I_session> session));
	MOCK_METHOD0(stop_all, void());

};