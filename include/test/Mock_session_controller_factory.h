#pragma once

#include "network/I_session_controller_factory.h"

class Mock_session_controller_factory : public I_session_controller_factory {

public:

	MOCK_METHOD0(create, std::shared_ptr<I_session_controller>());

};