#pragma once

#include "network/I_session_factory.h"

class Mock_session_factory : public I_session_factory {

public:

	MOCK_METHOD4(create, std::shared_ptr<I_session>(
		std::shared_ptr<I_session_controller> session_controller,
		std::shared_ptr<I_socket> socket,
		std::shared_ptr<I_circular_buffer<Packet>> receive_buffer,
		std::shared_ptr<I_circular_buffer<Packet>> send_buffer));

};