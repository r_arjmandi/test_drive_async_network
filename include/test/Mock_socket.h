#pragma once

#include "network/I_socket.h"

class Mock_socket : public I_socket {

public:

	MOCK_METHOD0(close, void());
	MOCK_METHOD0(shutdown, void());
	MOCK_METHOD3(async_connect, 
		void (const std::string&, int, const On_connect_function_type&));
	MOCK_METHOD2(async_write, 
		void(const Packet&, const On_async_write_function_type&));
	MOCK_METHOD2(async_read, 
		void(Packet&, const On_async_read_function_type&));
	MOCK_METHOD0(get_executor, I_executor<I_IO_context>&());

};