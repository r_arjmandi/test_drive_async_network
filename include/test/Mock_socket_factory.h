#pragma once

#include "network/I_socket_factory.h"

class Mock_socket_factory : public I_socket_factory {

public:

	MOCK_METHOD1(create, std::shared_ptr<I_socket>(
		std::shared_ptr<I_IO_context> IO_context_));

};