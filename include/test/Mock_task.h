#pragma once

#include "concurrency/I_task.h"

class Mock_task : public I_task {

public:
	
	MOCK_METHOD1(paranthese_op, bool (std::future<bool>));

	bool operator()(std::future<bool> is_task_stopped) override
	{
		return paranthese_op(std::move(is_task_stopped));
	}

};