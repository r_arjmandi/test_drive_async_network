#pragma once

#include "concurrency/I_task_handler.h"

class Mock_task_handler : public I_task_handler {

public:
	
	MOCK_METHOD0(stop, void ());
	MOCK_METHOD0(get, bool ());

};