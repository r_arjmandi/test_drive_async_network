#pragma once

#include "concurrency/I_task_runner.h"

class Mock_task_runner : public I_task_runner {

public:

	MOCK_METHOD1(run_async, 
		std::shared_ptr<I_task_handler> (std::shared_ptr<I_task>));
	MOCK_METHOD1(run_async, 
		std::shared_ptr<I_task_handler> 
			(std::function<bool(std::future<bool> is_task_stopped)>));

};