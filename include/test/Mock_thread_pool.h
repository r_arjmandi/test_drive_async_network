#pragma once

#include "concurrency/I_thread_pool.h"

class Mock_thread_pool : public I_thread_pool {

public:
	
	MOCK_METHOD0(join, void());
	MOCK_METHOD0(stop, void());
	MOCK_METHOD0(get_executor, I_executor<I_thread_pool>&());

};