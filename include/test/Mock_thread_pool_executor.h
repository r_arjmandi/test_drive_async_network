#pragma once

#include "concurrency/I_executor.h"

#include "concurrency/I_thread_pool.h"

class Mock_thread_pool_executor : public I_executor<I_thread_pool> {

public:

	MOCK_METHOD1(post, void(std::function<void()> func));
	MOCK_METHOD0(context, I_thread_pool& ());

};