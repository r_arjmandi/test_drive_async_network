#pragma once

#include "concurrency/I_thread_pool_factory.h"

class Mock_thread_pool_factory : public I_thread_pool_factory {

public:

	MOCK_METHOD1(
		create, std::shared_ptr<I_thread_pool>(
			unsigned int number_of_thread));

};