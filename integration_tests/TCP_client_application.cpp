#include "tcp_client_pch.h"

#include "TCP_client_application.h"

#include "network/Session_factory.h"
#include "network/TCP_client.h"

#include "network/Write_istream_to_send_buffer.h"
#include "network/Write_receive_buffer_to_ostream.h"

TCP_client_application::TCP_client_application(
	const std::string& IP_address,
	int port,
	bool send,
	bool receive,
	unsigned long long int send_buffer_size,
	unsigned long long int receive_buffer_size) :
	_send {send},
	_receive {receive}
{
	std::fill(_end_packet.begin(), _end_packet.end(), '0');
	_client = std::make_shared<TCP_client>(
		_IO_context,
		_socket_factory,
		_session_factory, 
		_session_controller_factory,
		_circular_buffer_factory, 
		send_buffer_size,
		receive_buffer_size);
	_client->async_connect(IP_address, port,
		[&](const auto& ec, auto session)
		{
			if (ec){
				return;
			}

			auto& executor {_IO_context->get_executor()};

			if (_send){
				std::string str;
				std::cin >> str;
				_istrstream = std::make_shared<std::istringstream>(str);
				std::make_shared<Write_istream_to_send_buffer>(
					*_istrstream, 
					executor, 
					session->get_send_buffer())->start(
						[&]()
						{
							_write_stdin_to_send_buffer_finished = true;
						});
			}
			if (_receive){
				std::make_shared<Write_receive_buffer_to_ostream>(
					std::cout, 
					executor, 
					session->get_receive_buffer())->start(
						[&]() 
						{
							_write_receive_buffer_to_ostream_finished = true;
						});
			}
		},
		_receive,
		_send,
		_end_packet,
		_end_packet);
}

void TCP_client_application::run()
{
	if (_start){
		return;
	}
	_start = true;
	_IO_context->run();
}

void TCP_client_application::stop()
{
	if (_start){
		_client->stop();
		_IO_context->stop();
		_start = false;
	}
}

TCP_client_application::~TCP_client_application()
{
	stop();
}