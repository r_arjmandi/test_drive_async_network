#pragma once

#include "concurrency/circular_buffer.h"
#include "concurrency/Task_runner.h"
#include "concurrency/circular_buffer_factory.h"
#include "concurrency/Thread_pool_factory.h"
#include "concurrency/Stop_task.h"
#include "concurrency/Task_runner.h"

#include "network/Session_factory.h"
#include "network/IO_context.h"
#include "network/I_TCP_client.h"
#include "network/Packet.h"
#include "network/Session_controller.h"
#include "network/Socket_factory.h"
#include "network/Session_controller_factory.h"

class TCP_client_application {

public:
	TCP_client_application(
		const std::string& IP_address,
		int port,
		bool send,
		bool receive,
		unsigned long long int send_buffer_size,
		unsigned long long int receive_buffer_size);
	~TCP_client_application();

	void run();
	void stop();

private:

	std::shared_ptr<I_TCP_client> _client;

	std::shared_ptr<IO_context> _IO_context {std::make_shared<IO_context>()};
	std::shared_ptr<IO_context> _IO_context2 {std::make_shared<IO_context>()};
	std::shared_ptr<Socket_factory> _socket_factory {
		std::make_shared<Socket_factory>()};
	std::shared_ptr<Session_factory> _session_factory {
		std::make_shared<Session_factory>()};
	std::shared_ptr<Session_controller_factory> _session_controller_factory {
		std::make_shared<Session_controller_factory>()};
	std::shared_ptr<circular_buffer_factory<Packet>> 
		_circular_buffer_factory {
			std::make_shared<circular_buffer_factory<Packet>>()};
	std::shared_ptr<std::istringstream> _istrstream;
	
	bool _start {false};
	bool _write_stdin_to_send_buffer_finished {false};
	bool _write_receive_buffer_to_ostream_finished {false};
	bool _send {false};
	bool _receive {false};
	Packet _end_packet;

};