#include "tcp_server_pch.h"

#include "TCP_server_application.h"

#include "network/Session_factory.h"
#include "network/TCP_server.h"
#include "network/Write_receive_buffer_to_ostream.h"
#include "network/Write_istream_to_send_buffer.h"

TCP_server_application::TCP_server_application(
	int port, 
	int number_of_send,
	int number_of_receive,
	unsigned long long int send_buffers_size_per_client,
	unsigned long long int receive_buffers_size_per_client,
	const fs::path& send_file_path,
	const fs::path& receive_file_path):
	_number_of_send {number_of_send},
	_number_of_receive {number_of_receive},
	_send_data_file_path {send_file_path},
	_receive_file_path {receive_file_path}
{
	std::fill(_end_packet.begin(), _end_packet.end(), '0');

	if (_number_of_receive > 0){
		_receive_file = std::make_shared<std::ofstream>(_receive_file_path);
	}

	_server = std::make_shared<TCP_server>(
		_IO_context,
		_socket_factory,
		_acceptor_factory,
		_session_factory,
		_session_controller_factory,
		_circular_buffer_factory,
		send_buffers_size_per_client,
		receive_buffers_size_per_client);

	_server->start_async_accept(port,
		[&](const auto& ec, auto session)
		{
			if (ec){
				return;
			}

			auto& executor {_IO_context2->get_executor()};
			if (_number_of_send > 0){
				_send_files.push_back(std::ifstream{_send_data_file_path});
				std::make_shared<Write_istream_to_send_buffer>(
					_send_files.back(), 
					executor, 
					session->get_send_buffer())->start(
						[&]()
						{
							_number_of_send_finished++;
						});
			}

			if (_number_of_receive > 0){
				std::make_shared<Write_receive_buffer_to_ostream>(
					*_receive_file, 
					executor, session->get_receive_buffer())->start(
						[&]()
						{
							_number_of_receive_finished++;
						});
			}

			if (_task_handler != nullptr) {
				return;
			}

			_task_handler = _task_runner.run_async(
				[&](std::shared_ptr<I_stop_token> stop_token)
				{
					while (!stop_token->is_stop()){
						if ((_number_of_receive_finished == 
							_number_of_receive)
							&& 
							(_number_of_send_finished == _number_of_send))
							{
								_server->stop_async_accept();
								break;
							}
						_IO_context2->restart();
						_IO_context2->run();
					}
					return true;
				});

		},
		_number_of_receive > 0, 
		_number_of_send > 0,
		_end_packet,
		_end_packet);
}

void TCP_server_application::run()
{
	if (_is_run){
		return;
	}
	_is_run = true;
	_IO_context->run();
}

TCP_server_application::~TCP_server_application()
{
	stop();
}

void TCP_server_application::stop()
{
	if (!_is_run){
		return;
	}
	_is_run = false;
	_server->stop_async_accept();
	_IO_context->stop();
	_task_handler->stop();
}