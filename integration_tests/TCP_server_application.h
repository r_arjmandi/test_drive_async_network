#pragma once

#include "concurrency/circular_buffer_factory.h"
#include "concurrency/Task_runner.h"
#include "concurrency/Thread_pool_factory.h"
#include "concurrency/Stop_task.h"

#include "network/Packet.h"
#include "network/I_TCP_server.h"
#include "network/IO_context_factory.h"
#include "network/Session_factory.h"
#include "network/Session_controller.h"
#include "network/Socket_factory.h"
#include "network/TCP_acceptor_factory.h"
#include "network/Session_controller_factory.h"
#include "network/IO_context.h"

class TCP_server_application {

public:

	TCP_server_application(
		int port, 
		int number_of_send,
		int number_of_receive,
		unsigned long long int send_buffers_size_per_client,
		unsigned long long int receive_buffers_size_per_client,
		const fs::path& send_file_path,
		const fs::path& receive_file_path);
	
	void run();

	~TCP_server_application();

private:

	void stop();

	std::shared_ptr<I_TCP_server> _server;

	std::shared_ptr<Session_factory> _session_factory {
		std::make_shared<Session_factory>()};
	std::shared_ptr<Socket_factory> _socket_factory {
		std::make_shared<Socket_factory>()};
	std::shared_ptr<TCP_acceptor_factory> _acceptor_factory {
		std::make_shared<TCP_acceptor_factory>()};
	std::shared_ptr<Session_controller_factory> _session_controller_factory {
		std::make_shared<Session_controller_factory>()};
	std::shared_ptr<circular_buffer_factory<Packet>> 
		_circular_buffer_factory {
			std::make_shared<circular_buffer_factory<Packet>>()};
	std::shared_ptr<IO_context> _IO_context {std::make_shared<IO_context>()};
	std::shared_ptr<IO_context> _IO_context2 {std::make_shared<IO_context>()};
	std::shared_ptr<I_task_handler> _task_handler {nullptr};
	std::shared_ptr<std::ofstream> _receive_file;

	Task_runner _task_runner;
	bool _is_run {false};
	bool _write_send_data_to_send_buffer_finished {false};
	bool _write_receive_buffer_to_std_out_finished {false};
	int _number_of_send_finished {0};
	int _number_of_receive_finished {0};
	int _number_of_send {0};
	int _number_of_receive {0};
	Packet _end_packet; 
	fs::path _receive_file_path;
	fs::path _send_data_file_path;
	std::vector<std::ifstream> _send_files; 

};