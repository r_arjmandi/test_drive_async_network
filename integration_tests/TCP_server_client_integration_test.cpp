#include "integration_tests_pch.h"

#include "config.h"
#include "test/Random_generator.h"

namespace bp = boost::process;

class TCP_server_client_integration_test : public testing::Test {

protected:

	struct SSH_info {

		std::string user;
		std::string remote_machine;

	};

	class Util {

	public:

		static void copy_via_scp(
			const std::string& source, const std::string& destination)
		{
			bp::child {"scp " + source + " " + destination}.wait();
		}

		static void delete_file_via_ssh(
			const SSH_info& ssh_info, const fs::path& file_path)
		{
			bp::child {
				"ssh " 
				+ ssh_info.user 
				+ "@" 
				+ ssh_info.remote_machine 
				+ " rm " 
				+ file_path.string()}.wait();
		}

		// static void delete_file_via_powershell(const fs::path& filePath)
		// {
		// 	bp::child delete_tcp_server_process{
		// 					"powershell del '" + filePath.string() + "'"};
		// 	delete_tcp_server_process.wait();
		// }

		// static void copy_file_via_powershell(
		// 	const fs::path& source, 
		// 	const fs::path& destination)
		// {
		// 	bp::child copy_file_to_server_process{
		// 		std::string {"powershell Copy-Item -Path "}
		// 		+ source.string() 
		// 		+ std::string {" -Destination '"} 
		// 		+ destination.string() 
		// 		+ std::string {"'"}};
		// 	copy_file_to_server_process.wait();
		// }

		// static void move_file_via_powershell(
		// 	const fs::path& source, 
		// 	const fs::path& destination)
		// {
		// 	auto command {std::string{"powershell Move-Item -Path "} 
		// 		+ source.string() 
		// 		+ std::string {" -Destination '"} 
		// 		+ destination.string() 
		// 		+ std::string {"'"}};
		// 	bp::child move_file_to_server_process {command};
		// 	move_file_to_server_process.wait();
		// }

		static void report(const std::string& message)
		{
			std::cout << "[  report  ] " << message << std::endl;
		}

		static std::string simplify_size(std::size_t bytes)
		{
			auto size {static_cast<long double>(bytes)};
			int count {0};
			for (; count < 6; count++){
				if (size / 1000.0 > 1){
					size = size / 1000.0;
				}
				else{
					break;
				}
			}
			std::array<std::string, 5> postfix {
				" Byte", " KB", " MB", " GB", " TB"};
			std::ostringstream sstr;
			sstr << std::fixed 
				 << std::setprecision(2) 
				 << size 
				 << postfix[count];
			return sstr.str();
		}

	};

	class Server {
	
	public:

		Server(std::size_t number_of_receive)
		{
			command_line_options.number_of_receive = number_of_receive;
			receive_data_from_clients = 
				std::vector<std::string> {number_of_receive};

			std::ofstream {_send_file_on_local_machine};
			std::ofstream {_receive_file_on_local_machine};
			command_line_options.send_file =
				 _server_root_path / _send_file_on_local_machine.filename();
			command_line_options.receive_file = 
				_server_root_path / _receive_file_on_local_machine.filename();
			Util::copy_via_scp(
				_server_executable_on_local_machine.string(), 
				_ssh_info.user 
				+ "@" 
				+ _ssh_info.remote_machine 
				+ ":"
				+ _server_executable.string());
		}

		void run()
		{
			create_server_command();

			if(send_data != ""){
				std::ofstream {_send_file_on_local_machine} << send_data;
			}

			Util::copy_via_scp(
				_send_file_on_local_machine.string(), 
				 _ssh_info.user 
				+ "@" 
				+ _ssh_info.remote_machine 
				+ ":"
				+ command_line_options.send_file.string());
			fs::remove(_send_file_on_local_machine);
			Util::copy_via_scp(
				_receive_file_on_local_machine.string(), 
				_ssh_info.user 
				+ "@" 
				+ _ssh_info.remote_machine 
				+ ":"
				+ command_line_options.receive_file.string());
			fs::remove(_receive_file_on_local_machine);
			_process = std::make_shared<bp::child>(_command);
		}

		void wait()
		{
			_process->wait();
			Util::copy_via_scp(
				_ssh_info.user 
				+ "@" 
				+ _ssh_info.remote_machine 
				+ ":"
				+ command_line_options.receive_file.string(), 
				_receive_file_on_local_machine);

			{
				std::ifstream receive_file {_receive_file_on_local_machine};
				
				auto i {0};
				while(receive_file >> receive_data_from_clients[i]){
					++i;
				}
			}

			fs::remove(_receive_file_on_local_machine);
		}

		~Server()
		{
			Util::delete_file_via_ssh(_ssh_info, _server_executable);
			Util::delete_file_via_ssh(_ssh_info, command_line_options.send_file);
			Util::delete_file_via_ssh(_ssh_info, 
				command_line_options.receive_file);
		}

		struct {
			int port {0};
			std::size_t number_of_send {0};
			std::size_t number_of_receive {0};
			std::size_t send_buffer_size_per_client {0};
			std::size_t receive_buffer_size_per_client {0};
			fs::path send_file;
			fs::path receive_file;
		} command_line_options;

		std::string send_data {""};
		std::vector<std::string> receive_data_from_clients;

	private:

		void create_server_command()
		{
			std::ostringstream command;
			command
				<< "ssh " 
				<< _ssh_info.user << "@" << _ssh_info.remote_machine << " " 
				<< _server_executable << " --Port=" 
				<< command_line_options.port
				<< " --Send=" << command_line_options.number_of_send
				<< " --Receive=" << command_line_options.number_of_receive
				<< " --SendBufferSizePerClient=" 
				<< command_line_options.send_buffer_size_per_client
				<< " --ReceiveBufferSizePerClient=" 
				<< command_line_options.receive_buffer_size_per_client 
				<< " --SendFile=" << command_line_options.send_file 
				<< " --ReceiveFile=" << command_line_options.receive_file;
			_command = command.str();
		}

		std::string _server_executable_name {"tcp_server" EXECUTABLE_SUFFIX};
		fs::path _send_file_on_local_machine {"Server_send_file.txt"};
		fs::path _receive_file_on_local_machine {"Server_receive_file.txt"};
		fs::path _server_root_path {"/home/rarjmandi"};
		fs::path _server_executable_on_local_machine {
			fs::path{
				"/home/rarjmandi/Desktop/test_drive_async_network"
				"/build/integration_tests"} 
				/ _server_executable_name};
		fs::path _server_executable {
			_server_root_path / _server_executable_name};
		std::shared_ptr<bp::child> _process;
		std::string _command;
		SSH_info _ssh_info {"rarjmandi", "192.168.1.2"};

	};

	struct TCP_client {
		struct {
			int port {0};
			std::string ip;
			bool send {false};
			bool receive {false};
			std::size_t send_buffer_size {0};
			std::size_t receive_buffer_size {0};
		} command_line_options;

		std::string send_data;
		std::string receive_data;
		bp::opstream std_in;
		bp::ipstream std_out;
		std::shared_ptr<bp::child> process;
		std::string command;
	};

	

	void create_clients_command(std::vector<TCP_client>& clients)
	{
		std::for_each(clients.begin(), clients.end(), 
			[](auto& elem)
			{
				std::ostringstream command;
				command
					<< "/home/rarjmandi/Desktop/test_drive_async_network/"
					"build/integration_tests/tcp_client" << EXECUTABLE_SUFFIX 
					<< " --Port=" << elem.command_line_options.port
					<< " --IP=" << elem.command_line_options.ip
					<< " --SendBufferSize=" 
					<< elem.command_line_options.send_buffer_size
					<< " --ReceiveBufferSize=" 
					<< elem.command_line_options.receive_buffer_size;
				if (elem.command_line_options.send){
					command << " --Send ";
				}
				if (elem.command_line_options.receive){
					command << " --Receive ";
				}
				elem.command = command.str();
			});
	}

	void run_clients(std::vector<TCP_client>& clients)
	{
		std::for_each(clients.begin(), clients.end(), 
			[&, counter = 0](auto& elem) mutable
			{
				elem.process = std::make_shared<bp::child>(
					elem.command, 
					bp::std_in < elem.std_in, 
					bp::std_out > elem.std_out);
			});
		Util::report("Clients is running");
	}

	void put_data_to_clients_std_in(std::vector<TCP_client>& clients)
	{
		std::for_each(clients.begin(), clients.end(), 
			[&, counter = 0](auto& elem) mutable
			{
				if (elem.command_line_options.send){
					elem.std_in << elem.send_data << std::endl;
					elem.std_in.close();
				}
			});
		Util::report("Data has pushed to clients input");
	}

	void read_data_from_clients_std_out(std::vector<TCP_client>& clients)
	{
		std::for_each(clients.begin(), clients.end(), 
			[&, counter = 0](auto& elem) mutable
			{
				if (elem.command_line_options.receive){
					elem.std_out >> elem.receive_data;
					elem.std_out.clear();
				}
			});
		Util::report("Data has read from clients output");
	}

	void wait_for_clients_exit(std::vector<TCP_client>& clients)
	{
		std::for_each(clients.begin(), clients.end(), [](auto& elem)
			{
				elem.process->wait();
			});
		Util::report("All clients has exited");
	}

	void run_server_and_clients(Server& server, std::vector<TCP_client>& clients)
	{
		server.run();
		Util::report("Server is running");

		std::this_thread::sleep_for(std::chrono::seconds {2});

		create_clients_command(clients);
		run_clients(clients);
		put_data_to_clients_std_in(clients);
		read_data_from_clients_std_out(clients);

		wait_for_clients_exit(clients);

		server.wait();
		Util::report("Server has exited");
	}

	std::string generate_random_data(int min, int max)
	{
		auto dataSize {_random_generator.generate_integer(min, max) * 64};
		return _random_generator.GenerateString(dataSize);
	}

	Random_generator _random_generator;
	const int port {1234};
	const std::string ip {"192.168.1.2"};

};

TEST_F(TCP_server_client_integration_test, 
	send_data_from_server_to_one_client)
{
	auto send_data_from_server {generate_random_data(1000, 1000000)};

	Server server {0};
	server.command_line_options.port = port;
	server.command_line_options.number_of_send = 1;
	server.command_line_options.send_buffer_size_per_client = 640000;
	server.command_line_options.receive_buffer_size_per_client = 0;
	server.send_data = send_data_from_server;

	std::vector<TCP_client> clients {1};
	clients[0].command_line_options.ip = ip;
	clients[0].command_line_options.port = port;
	clients[0].command_line_options.receive = true;
	clients[0].command_line_options.send = false;
	clients[0].command_line_options.receive_buffer_size = 64000;
	clients[0].command_line_options.send_buffer_size = 0;

	Util::report(Util::simplify_size(
		send_data_from_server.size()) + " is going to transferred");
	run_server_and_clients(server, clients);

	ASSERT_THAT(send_data_from_server, testing::Eq(clients[0].receive_data));
}

TEST_F(TCP_server_client_integration_test, 
	send_data_from_one_client_to_server)
{
	auto send_data_from_client {generate_random_data(1000, 1000000)};

	Server server {1};
	server.command_line_options.port = port;
	server.command_line_options.number_of_send = 0;
	server.command_line_options.send_buffer_size_per_client = 0;
	server.command_line_options.receive_buffer_size_per_client = 640000;

	std::vector<TCP_client> clients {1};
	clients[0].command_line_options.ip = ip;
	clients[0].command_line_options.port = port;
	clients[0].command_line_options.receive = false;
	clients[0].command_line_options.send = true;
	clients[0].command_line_options.receive_buffer_size = 0;
	clients[0].command_line_options.send_buffer_size = 64000;
	clients[0].send_data = send_data_from_client;

	Util::report(Util::simplify_size(
		send_data_from_client.size()) + " is going to transferred");
	run_server_and_clients(server, clients);

	ASSERT_THAT(send_data_from_client, 
		testing::Eq(server.receive_data_from_clients[0]));
}

TEST_F(TCP_server_client_integration_test, 
	transfer_data_between_server_and_one_client_in_both_direction)
{
	auto send_data_from_client {generate_random_data(1000, 1000000)};
	auto send_data_from_server {generate_random_data(1000, 1000000)};

	Server server {1};
	server.command_line_options.port = port;
	server.command_line_options.number_of_send = 1;
	server.command_line_options.send_buffer_size_per_client = 640000;
	server.command_line_options.receive_buffer_size_per_client = 640000;
	server.send_data = send_data_from_server;

	std::vector<TCP_client> clients {1};
	clients[0].command_line_options.ip = ip;
	clients[0].command_line_options.port = port;
	clients[0].command_line_options.receive = true;
	clients[0].command_line_options.send = true;
	clients[0].command_line_options.receive_buffer_size = 64000;
	clients[0].command_line_options.send_buffer_size = 64000;
	clients[0].send_data = send_data_from_client;

	Util::report(Util::simplify_size(
		send_data_from_client.size() 
		+ send_data_from_server.size()) 
		+ " is going to transferred");
	run_server_and_clients(server, clients);

	ASSERT_THAT(send_data_from_client, 
		testing::Eq(server.receive_data_from_clients[0]));
	ASSERT_THAT(send_data_from_server, testing::Eq(clients[0].receive_data));
}

TEST_F(TCP_server_client_integration_test, 
	transfer_data_between_server_and_some_client_in_both_direction)
{
	auto number_of_clients {
		_random_generator.generate_integer<std::size_t>(1, 1)};

	std::vector<std::string> send_data_from_clients {number_of_clients};
	std::for_each(
		send_data_from_clients.begin(), 
		send_data_from_clients.end(),
		[&](auto& elem) mutable
		{
			elem = generate_random_data(1, 10);
		});

	auto send_data_from_server {generate_random_data(1, 10)};

	Server server {number_of_clients};
	server.command_line_options.port = port;
	server.command_line_options.number_of_send = number_of_clients;
	server.command_line_options.send_buffer_size_per_client = 6400;
	server.command_line_options.receive_buffer_size_per_client = 6400;
	server.send_data = send_data_from_server;

	std::vector<TCP_client> clients {number_of_clients};
	std::for_each(clients.begin(), clients.end(),
	[&, counter {0}](auto& elem) mutable
	{
		elem.command_line_options.ip = ip;
		elem.command_line_options.port = port;
		elem.command_line_options.receive = true;
		elem.command_line_options.send = true;
		elem.command_line_options.receive_buffer_size = 6400;
		elem.command_line_options.send_buffer_size = 6400;
		elem.send_data = send_data_from_clients[counter];
		counter++;
	});

	Util::report(std::to_string(number_of_clients) + " clients connected");

	long long int sentDataFromClientsSize {0};
	std::for_each(
		send_data_from_clients.cbegin(), 
		send_data_from_clients.cend(),
		[&](const auto& elem) 
		{
			sentDataFromClientsSize += elem.size(); 
		});
	auto dataSizeSum {
		sentDataFromClientsSize 
		+ send_data_from_server.size() * number_of_clients};
	Util::report(Util::simplify_size(
		dataSizeSum) + " is going to transferred");
	run_server_and_clients(server, clients);

	ASSERT_THAT(server.receive_data_from_clients, 
		testing::UnorderedElementsAreArray(send_data_from_clients));

	std::vector<std::string> clients_received_data {number_of_clients};
	std::for_each(clients_received_data.begin(), clients_received_data.end(),
		[&, counter {0}](auto& elem) mutable 
		{
			elem = clients[counter++].receive_data; 
		});
	ASSERT_THAT(clients_received_data, testing::Each(send_data_from_server));
}