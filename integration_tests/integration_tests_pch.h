#ifndef INTEGRATION_TESTS_PCH
#define INTEGRATION_TESTS_PCH

#include <random>
#include <cstdio>
#include <chrono>
#include <filesystem>
#include <numeric>
#include <sstream>
#include <functional>
#include <iostream>
#include <memory>
#include <future>
#include <string>
#include <fstream>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <boost/process.hpp>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include "boost/circular_buffer.hpp"

#ifdef WIN32
    namespace fs = std::experimental::filesystem;
#else
    namespace fs = std::filesystem;
#endif

#endif