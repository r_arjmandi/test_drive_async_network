#include "tcp_client_pch.h"

#include "TCP_client_application.h"

namespace po = boost::program_options;

bool is_variable_exist(
	const po::variables_map& variable_map, const std::string& var)
{
	if (!variable_map.count(var)){
		return false;
	}
	return true;
}

int main(int argc, char ** argv)
{
	auto descriptions {po::options_description()};
	descriptions.add_options()
		("help", "Print all commands and usage")
		("IP", po::value<std::string>(), "Set the IP address")
		("Port", po::value<int>(), "Set the Port number")
		("Send", "TCP_client send data to endpoint")
		("Receive", "TCP_client receive data from endpoint")
		("SendBufferSize", po::value<unsigned long long int>(), 
			"Set the send buffer size in byte")
		("ReceiveBufferSize", po::value<unsigned long long int>(), 
			"Set the receive buffer size in byte");
	auto variable_maps {po::variables_map()};
	po::store(
		po::parse_command_line(argc, argv, descriptions), variable_maps);
	po::notify(variable_maps);

	if (variable_maps.count("help")){
		std::cout << descriptions << std::endl;
	}

	for(const auto& var : 
		{"IP", 
		"Port", 
		"SendBufferSize", 
		"ReceiveBufferSize" }){
		if(!is_variable_exist(variable_maps, var)){
			std::cerr << "The --" << var << " must be set";
			return -1;
		}
	}

	TCP_client_application application(
		variable_maps["IP"].as<std::string>(),
		variable_maps["Port"].as<int>(),
		variable_maps.count("Send") == 0 ? false : true,
		variable_maps.count("Receive") == 0 ? false : true,
		variable_maps["SendBufferSize"].as<unsigned long long int>(),
		variable_maps["ReceiveBufferSize"].as<unsigned long long int>());

	application.run();
	return 0;
}