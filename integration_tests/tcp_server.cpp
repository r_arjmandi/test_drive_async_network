#include "tcp_server_pch.h"

#include "TCP_server_application.h"

namespace po = boost::program_options;

bool is_variable_exist(
	const po::variables_map& variable_map, const std::string& var)
{
	if (!variable_map.count(var)){
		return false;
	}
	return true;
}

int main(int argc, char** argv)
{
	auto descriptions {po::options_description()};
	descriptions.add_options()
		("help", "Print all commands and usage")
		("Port", po::value<int>(), "Set the Port number")
		("Send", po::value<int>(), 
			"The number of clients that server must send data to them")
		("Receive", po::value<int>(), 
			"The number of clients that server must receive data from them")
		("SendBufferSizePerClient", po::value<unsigned long long int>(), 
			"Set the send buffer size per client in byte")
		("ReceiveBufferSizePerClient", po::value<unsigned long long int>(), 
			"Set the receive buffer size per client in byte")
		("SendFile", po::value<fs::path>(), "Set sending file path")
		("ReceiveFile", po::value<fs::path>(), "Set receiving file path");
	auto variable_maps {po::variables_map()};
	po::store(
		po::parse_command_line(argc, argv, descriptions), variable_maps);
	po::notify(variable_maps);

	if (variable_maps.count("help")){
		std::cout << descriptions << std::endl;
	}

	for(const auto& var : 
		{"Port", 
		"SendBufferSizePerClient", 
		"ReceiveBufferSizePerClient", 
		"Send", 
		"Receive", 
		"SendFile", 
		"ReceiveFile"}){
		if(!is_variable_exist(variable_maps, var)){
			std::cerr << std::string {"The --"} 
			+ var 
			+ std::string {" must be set"};
			return -1;
		}
	}

	TCP_server_application application{
		variable_maps["Port"].as<int>(),
		variable_maps["Send"].as<int>(),
		variable_maps["Receive"].as<int>(),
		variable_maps[
			"SendBufferSizePerClient"].as<unsigned long long int>(),
		variable_maps[
			"ReceiveBufferSizePerClient"].as<unsigned long long int>(),
		variable_maps["SendFile"].as<fs::path>(),
		variable_maps["ReceiveFile"].as<fs::path>()};
	application.run();

	return 0;
}