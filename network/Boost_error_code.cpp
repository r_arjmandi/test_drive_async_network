#include "pch.h"

#include "Boost_error_code.h"

Boost_error_code::Boost_error_code(const boost::system::error_code& ec):
	_ec(ec)
{
}

std::string Boost_error_code::message() const
{
	return _ec.message();
}

int Boost_error_code::value() const
{
	return _ec.value();
}

Boost_error_code::operator bool() const
{
	return _ec.failed();
}
