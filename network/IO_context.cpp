#include "pch.h"

#include "IO_context.h"

#include "TCP_acceptor.h"
#include "TCP_socket.h"

IO_context::IO_context():
	_executor(*this)
{
}

int IO_context::run()
{
	return static_cast<int>(_IO_context.run());
}

int IO_context::run_one()
{
	return static_cast<int>(_IO_context.run_one());
}

int IO_context::run_for(const std::chrono::milliseconds& miliSeconds)
{
	return static_cast<int>(_IO_context.run_for(miliSeconds));
}

void IO_context::stop()
{
	_IO_context.stop();
}

void IO_context::restart()
{
	_IO_context.restart();
}

I_executor<I_IO_context>& IO_context::get_executor()
{
	return _executor;
}

boost::asio::io_context& IO_context::get_native_IO_context()
{
	return _IO_context;
}

IO_context::IO_executor::IO_executor(IO_context& ioContext):
	_context(ioContext)
{
}

void IO_context::IO_executor::post(std::function<void()> func)
{
	boost::asio::post(_context.get_native_IO_context(), func);
}

I_IO_context& IO_context::IO_executor::context()
{
	return _context;
}