#include "pch.h"

#include "IO_context_factory.h"
#include "IO_context.h"

std::shared_ptr<I_IO_context> IO_context_factory::create()
{
	return std::make_shared<IO_context>();
}
