#include "pch.h"

#include "Session.h"

Session::Session(
	std::shared_ptr<I_session_controller> session_controller,
	std::shared_ptr<I_socket> socket,
	std::shared_ptr<I_circular_buffer<Packet>> receive_buffer,
	std::shared_ptr<I_circular_buffer<Packet>> send_buffer) :
	_session_controller(session_controller),
	_socket(socket),
	_receive_buffer(receive_buffer),
	_send_buffer(send_buffer)
{
}

Session::~Session()
{
	stop();
	_socket->close();
}

void Session::start(
	bool enable_receive, 
	bool enable_send, 
	Packet& end_receive_packet, 
	Packet& end_send_packet)
{
	if (!_is_session_stopped){
		return;
	}
	
	_end_receive_packet = end_receive_packet;
	_end_send_packet = end_send_packet;
	_is_session_stopped = false;

	if(enable_receive){
		start_async_read();
	}

	if(enable_send){
		start_async_write();
	}
}

void Session::stop()
{
	if (!_is_session_stopped){
		_is_session_stopped = true;
	}
}

std::shared_ptr<I_circular_buffer<Packet>> Session::get_send_buffer() const
{
	return _send_buffer;
}

std::shared_ptr<I_circular_buffer<Packet>> Session::get_receive_buffer() const
{
	return _receive_buffer;
}

void Session::start_async_read()
{
	_socket->async_read(_read_packet, 
		[self = shared_from_this()](const auto& ec)
	{
		if (ec){
			self->_session_controller->stop(self);
			return;
		}
		self->try_push_to_receive_buffer();
	});
}

void Session::try_push_to_receive_buffer()
{
	if (_receive_buffer->full()){
		auto& executor = _socket->get_executor();
		executor.post(
			[self = shared_from_this()]() 
			{
				self->try_push_to_receive_buffer();
			});
		return;
	}
	_receive_buffer->push_front(_read_packet);
	if(_read_packet == _end_receive_packet){
		return;
	}
	start_async_read();
}

void Session::start_async_write()
{
	if(_is_session_stopped){
		return;
	}

	if (_send_buffer->empty()){
		auto& executor = _socket->get_executor();
		executor.post(
			[self = shared_from_this()]() 
			{
				self->start_async_write(); 
			});
		return;
	}

	_write_packet = _send_buffer->back();
	_socket->async_write(_write_packet, 
		[self = shared_from_this()](const auto& ec)
	{
		if (ec){
			self->_session_controller->stop(self);
			return;
		}

		self->_send_buffer->pop_back();
		if(self->_write_packet == self->_end_send_packet){
			return;
		}
		self->start_async_write();
	});
}