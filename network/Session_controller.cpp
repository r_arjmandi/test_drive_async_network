#include "pch.h"

#include "network/Session_controller.h"

Session_controller::~Session_controller()
{
	if (_sessions.empty()){
		return;
	}
	stop_all();
}

void Session_controller::start(
	std::shared_ptr<I_session> session,
	bool enable_receive, 
	bool enable_send, 
	Packet& end_receive_packet, 
	Packet& end_send_packet)
{
	if (_sessions.find(session) == _sessions.end()){
		_sessions.insert(session);
		session->start(
			enable_receive, 
			enable_send, 
			end_receive_packet, 
			end_send_packet);
	}
}

void Session_controller::stop(std::shared_ptr<I_session> session)
{
	auto elem = _sessions.find(session);
	if (elem == _sessions.end()){
		return;
	}
	(*elem)->stop();
	_sessions.erase(elem);
}

void Session_controller::stop_all()
{
	for (const auto& session : _sessions){
		session->stop();
	}
	_sessions.clear();
}