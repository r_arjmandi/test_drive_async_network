#include "pch.h"

#include "network/Session_controller_factory.h"

#include "network/Session_controller.h"

std::shared_ptr<I_session_controller> Session_controller_factory::create()
{
	return std::make_shared<Session_controller>();
}
