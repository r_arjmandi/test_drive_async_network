#include "pch.h"

#include "Session_factory.h"
#include "Session.h"

std::shared_ptr<I_session> Session_factory::create(
	std::shared_ptr<I_session_controller> session_controller,
	std::shared_ptr<I_socket> socket,
	std::shared_ptr<I_circular_buffer<Packet>> receive_buffer,
	std::shared_ptr<I_circular_buffer<Packet>> send_buffer)
{
	return std::make_shared<Session>(
		session_controller, socket, receive_buffer, send_buffer);
}