#include "pch.h"

#include "Socket_factory.h"

#include "TCP_socket.h"

std::shared_ptr<I_socket> Socket_factory::create(
	std::shared_ptr<I_IO_context>  IO_context_)
{
		return std::make_shared<TCP_socket>(IO_context_);
}