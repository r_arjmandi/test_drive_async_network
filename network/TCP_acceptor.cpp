#include "pch.h"

#include "TCP_acceptor.h"

#include "Boost_error_code.h"
#include "TCP_socket.h"
#include "IO_context.h"

TCP_acceptor::TCP_acceptor(
	std::shared_ptr<I_IO_context> IO_context_, int port):
	_IO_context(IO_context_),
	_acceptor(
		std::dynamic_pointer_cast<IO_context>(
			IO_context_)->get_native_IO_context(),
		boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
{
}

void TCP_acceptor::async_accept(
	std::shared_ptr<I_socket> socket,
	const On_accept_function_type& on_accept)
{
	auto tcp_socket = std::dynamic_pointer_cast<TCP_socket>(socket);
	if (!tcp_socket){
		return;
	}

	_acceptor.async_accept(tcp_socket->get_native_socket(),
		[on_accept, socket](const auto& ec)
		{
			Boost_error_code errorCode(ec);
			on_accept(errorCode);
		});
}

void TCP_acceptor::close()
{
	_acceptor.close();
}

void TCP_acceptor::cancel()
{
	_acceptor.cancel();
}

void TCP_acceptor::release()
{
	_acceptor.release();
}