#include "pch.h"

#include "network/TCP_acceptor_factory.h"

#include "network/TCP_acceptor.h"

std::shared_ptr<I_TCP_acceptor> TCP_acceptor_factory::create(
	std::shared_ptr<I_IO_context> IO_context_, int port)
{
	return std::make_shared<TCP_acceptor>(IO_context_, port);
}
