#include "pch.h"

#include "TCP_client.h"

TCP_client::TCP_client(
	std::shared_ptr<I_IO_context> IO_context,
	std::shared_ptr<I_socket_factory> socket_factory,
	std::shared_ptr<I_session_factory> session_factory,
	std::shared_ptr<I_session_controller_factory> session_controller_factory,
	std::shared_ptr<I_circular_buffer_factory<Packet>> circular_buffer_factory,
	unsigned long long int send_buffer_size,
	unsigned long long int receive_buffer_size):
	_IO_context(IO_context),
	_socket_factory(socket_factory),
	_session_factory(session_factory),
	_circular_buffer_factory(circular_buffer_factory),
	_send_buffer_size(send_buffer_size),
	_receive_buffer_size(receive_buffer_size)
{
	_session_controller = session_controller_factory->create();
}

TCP_client::~TCP_client()
{
	stop();
}

void TCP_client::async_connect(
	const std::string& IP_address, 
	int port, 
	On_connect_function_type on_connect,
	bool enable_receive, 
	bool enable_send, 
	Packet& end_receive_packet, 
	Packet& end_send_packet)
{
	if (_is_run){
		return;
	}

	_enable_receive = enable_receive; 
	_enable_send = enable_send;
	_end_receive_packet = end_receive_packet; 
	_end_send_packet = end_send_packet;
	_on_connect = on_connect;
	_socket = _socket_factory->create(_IO_context);
	_socket->async_connect(
		IP_address, 
		port, 
		std::bind(
			&TCP_client::socket_on_connect, 
			this, 
			std::placeholders::_1));
	_is_run = true;
}

void TCP_client::stop()
{
	if (_is_session_started){
		_session_controller->stop_all();
		_is_session_started = false;
	}
	if (_is_run){
		_is_run = false;
	}
}

void TCP_client::socket_on_connect(const I_error_code& error_code)
{
	if (!error_code){
		auto receive_buffer = 
			_circular_buffer_factory->create(_receive_buffer_size);
		auto send_buffer = 
			_circular_buffer_factory->create(_send_buffer_size);
		auto session = 
			_session_factory->create(
				_session_controller, 
				_socket, 
				receive_buffer, 
				send_buffer);
		_session_controller->start(
			session, 
			_enable_receive, 
			_enable_send, 
			_end_receive_packet, 
			_end_send_packet);
		_is_session_started = true;
		_on_connect(error_code, session);
	}
}
