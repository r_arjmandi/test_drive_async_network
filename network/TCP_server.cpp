#include "pch.h"

#include "TCP_server.h"

TCP_server::TCP_server(
	std::shared_ptr<I_IO_context> IO_context,
	std::shared_ptr<I_socket_factory> socket_factory,
	std::shared_ptr<I_TCP_acceptor_factory> acceptor_factory,
	std::shared_ptr<I_session_factory> session_factory,
	std::shared_ptr<I_session_controller_factory> session_controller_factory,
	std::shared_ptr<I_circular_buffer_factory<Packet>> circular_buffer_factory,
	unsigned long int send_buffer_size_per_client,
	unsigned long int receive_buffer_size_per_client) :
	_IO_context {IO_context},
	_socket_factory {socket_factory},
	_acceptor_factory {acceptor_factory},
	_session_factory {session_factory},
	_circular_buffer_factory {circular_buffer_factory},
	_send_buffer_size_per_client {send_buffer_size_per_client},
	_receive_buffer_size_per_client {receive_buffer_size_per_client}
{
	_session_controller = session_controller_factory->create();
}

TCP_server::~TCP_server()
{
	if(_session_is_started){
		_session_controller->stop_all();
	}
	stop_async_accept();
}

void TCP_server::start_async_accept(
	int port, 
	const On_accept_function_type& on_accept,
	bool enable_receive, 
	bool enable_send, 
	Packet& end_receive_packet, 
	Packet& end_send_packet)
{
	if (_is_async_accept_started){
		return;
	}

	_enable_receive = enable_receive; 
	_enable_send = enable_send;
	_end_receive_packet = end_receive_packet; 
	_end_send_packet = end_send_packet;
	_is_async_accept_started = true;
	_acceptor = _acceptor_factory->create(_IO_context, port);
	_on_accept = on_accept;
	DoAccept();
}

void TCP_server::stop_async_accept()
{
	if (_is_async_accept_started){
		_acceptor->close();
		_is_async_accept_started = false; 
	}
}

void TCP_server::DoAccept()
{
	if (!_is_async_accept_started){
		return;
	}
	auto socket = _socket_factory->create(_IO_context);
	_acceptor->async_accept(
		socket,
		std::bind(
			&TCP_server::on_accept, 
			this, 
			socket,
			_IO_context,
			std::placeholders::_1));
}

void TCP_server::on_accept(
	std::shared_ptr<I_socket> socket, 
	std::shared_ptr<I_IO_context> socketIOContext, 
	const I_error_code& ec)
{
	if (!_is_async_accept_started){
		return;
	}
	std::shared_ptr<I_session> session = nullptr;
	if (!ec)
	{
		auto receive_buffer = 
			_circular_buffer_factory->create(_receive_buffer_size_per_client);
		auto send_buffer = 
			_circular_buffer_factory->create(_send_buffer_size_per_client);
		session = 
			_session_factory->create(
				_session_controller, 
				socket, 
				receive_buffer, 
				send_buffer);
		_session_controller->start(
			session, 
			_enable_receive, 
			_enable_send, 
			_end_receive_packet, 
			_end_send_packet);
		_session_is_started = true;
	}
	DoAccept();
	_on_accept(ec, session);
}