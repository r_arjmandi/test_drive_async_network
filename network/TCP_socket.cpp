#include "pch.h"

#include "TCP_socket.h"

#include "Boost_error_code.h"
#include "IO_context.h"

TCP_socket::TCP_socket(std::shared_ptr<I_IO_context> ioContext):
	_IO_context {ioContext},
	_socket{
		std::dynamic_pointer_cast<IO_context>(ioContext)->get_native_IO_context()}
{
}

TCP_socket::~TCP_socket()
{
	shutdown();
	close();
}

void TCP_socket::close()
{
	_socket.close();
}

void TCP_socket::shutdown()
{
	boost::system::error_code ignored_ec;
	_socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both,
		ignored_ec);
}

void TCP_socket::async_connect(
	const std::string& IP_address, 
	int port, 
	const On_connect_function_type& on_connect)
{
	boost::system::error_code boostEC;
	const auto address = boost::asio::ip::make_address(IP_address, boostEC);
	if (!boostEC){
		_socket.async_connect(
			boost::asio::ip::tcp::endpoint(address, port),
			[on_connect](auto ec)
			{
				const auto errorCode = Boost_error_code(ec);
				on_connect(errorCode);
			});
		return;
	}
	const auto errorCode = Boost_error_code(boostEC);
	on_connect(errorCode);
}

void TCP_socket::async_write(
	const Packet& packet,
	const On_async_write_function_type& on_async_write)
{
	boost::asio::async_write(
		_socket, boost::asio::buffer(&packet, packet.size()),
		[on_async_write](auto ec, auto)
		{
			const auto errorCode = Boost_error_code(ec);
			on_async_write(errorCode);
		});
}

void TCP_socket::async_read(
	Packet& packet, 
	const On_async_read_function_type& on_async_read)
{
	boost::asio::async_read(
		_socket, boost::asio::buffer(&packet, packet.size()),
		[on_async_read, this](auto ec, auto)
		{
			const auto errorCode = Boost_error_code(ec);
			on_async_read(errorCode);
		});
}

I_executor<I_IO_context>& TCP_socket::get_executor()
{
	return _IO_context->get_executor();
}

boost::asio::ip::tcp::socket& TCP_socket::get_native_socket()
{
	return _socket;
}