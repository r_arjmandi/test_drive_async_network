#include "pch.h"
#include "Write_istream_to_send_buffer.h"

Write_istream_to_send_buffer::Write_istream_to_send_buffer(
	std::istream& IStream, 
	I_executor<I_IO_context>& executor, 
	std::shared_ptr<I_circular_buffer<Packet>> send_buffer) :
	_istream(IStream),
	_executor(executor),
	_send_buffer(send_buffer)
{
	std::fill(_end_packet.begin(), _end_packet.end(), '0');
}

void Write_istream_to_send_buffer::start(
		const std::function<void(void)>& finishHandler)
{
	_finish_handler = finishHandler;
	_executor.post(
		[self = shared_from_this()]() 
		{
			self->read_a_packet(); 
		});
}

void Write_istream_to_send_buffer::read_a_packet()
{
	if(_istream.read(
		reinterpret_cast<char *>(_packet.data()), _packet.size())){
 		push_to_send_buffer(_packet);
	}
	else{
		push_to_send_buffer(_end_packet);
	}
}

inline void Write_istream_to_send_buffer::push_to_send_buffer(
	const Packet& packet)
{
	if (_send_buffer->full()){
		_executor.post(
			[self = shared_from_this(), &packet]() 
			{
				self->push_to_send_buffer(packet);
			});
	}
	else{
		_send_buffer->push_front(packet);
		if(packet != _end_packet){
			_executor.post(
				[self = shared_from_this()]() 
				{
					self->read_a_packet(); 
				});
		}
		else{
			try_run_finish_handler();
		}
	}
}

void Write_istream_to_send_buffer::try_run_finish_handler()
{
	if (_send_buffer->empty()){
		_executor.post(
			[self = shared_from_this()]() 
			{
				self->_finish_handler(); 
			});
	}
	else{
		_executor.post(
			[self = shared_from_this()]() 
			{
				self->try_run_finish_handler(); 
			});
	}
}
