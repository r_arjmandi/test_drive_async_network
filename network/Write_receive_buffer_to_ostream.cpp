#include "pch.h"

#include "Write_receive_buffer_to_ostream.h"

Write_receive_buffer_to_ostream::Write_receive_buffer_to_ostream(
	std::ostream& ostream,
	I_executor<I_IO_context>& executor,
	std::shared_ptr<I_circular_buffer<Packet>> receive_buffer) :
	_ostream {ostream},
	_executor {executor},
	_receive_buffer {receive_buffer}
{
	std::fill(_end_packet.begin(), _end_packet.end(), '0');
}

void Write_receive_buffer_to_ostream::start(
	const std::function<void()>& finish_handler)
{
	_finish_handler = finish_handler;
	_executor.post(
		[self = shared_from_this()]() 
		{
			self->read_from_receive_buffer(); 
		});
}

void Write_receive_buffer_to_ostream::read_from_receive_buffer()
{
	if(!_receive_buffer->empty()){
		_packet = _receive_buffer->back();
		if (_packet == _end_packet){
			_receive_buffer->pop_back();
			_ostream << _data.str() << std::endl;
			_finish_handler(); 
			return;
		}
		_data.write(
			reinterpret_cast<const char*>(_packet.data()), _packet.size());
		_receive_buffer->pop_back();
	}
	_executor.post(
		[self = shared_from_this()]() 
		{
			self->read_from_receive_buffer(); 
		});
}