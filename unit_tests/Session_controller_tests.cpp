#include "pch.h"

#include "network/Session_controller.h"

#include "test/Mock_session.h"
#include "test/Mock_IO_context.h"
#include "test/Mock_thread_pool_factory.h"
#include "test/Mock_thread_pool.h"
#include "test/Mock_thread_pool_executor.h"
#include "test/Random_generator.h"

using namespace testing;

struct Session_controller_tests : public Test {

protected:

	void SetUp() override
	{
		_session_controller = std::make_shared<Session_controller>();
	}

	std::shared_ptr<Session_controller> _session_controller;

	std::array<std::shared_ptr<Mock_session>, 3> _sessions {
		std::make_shared<Mock_session>(),
		std::make_shared<Mock_session>(),
		std::make_shared<Mock_session>()
	};

	Random_generator _random_generator;
	std::array<bool, 3> _enable_receive {
		_random_generator.generate_bool(),
		_random_generator.generate_bool(),
		_random_generator.generate_bool()
	};

	std::array<bool, 3> _enable_send {
		_random_generator.generate_bool(),
		_random_generator.generate_bool(),
		_random_generator.generate_bool()
	};

	std::array<Packet, 3> _end_receive_packet {
		_random_generator.generate_packet(),
		_random_generator.generate_packet(),
		_random_generator.generate_packet()
	}; 

	std::array<Packet, 3> _end_send_packet {
		_random_generator.generate_packet(),
		_random_generator.generate_packet(),
		_random_generator.generate_packet()
	};

};

TEST_F(Session_controller_tests, 
when_start_is_called_with_a_session_then_session_must_be_started)
{
	Expectation start_session = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);
	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);
	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(start_session);
}

TEST_F(Session_controller_tests, 
when_stop_is_called_with_a_session_then_session_must_be_stopped)
{
	Expectation start_session = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);
	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);

	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(start_session);
	_session_controller->stop(_sessions[0]);
}

TEST_F(Session_controller_tests, 
when_stop_all_is_called_with_a_session_then_all_session_must_be_stopped)
{
	Expectation start_session1 = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);
	Expectation start_session2 = 
		EXPECT_CALL(*_sessions[1], 
			start(
				_enable_receive[1], 
				_enable_send[1], 
				_end_receive_packet[1], 
				_end_send_packet[1])).Times(1).After(start_session1);
	Expectation start_session3 = 
		EXPECT_CALL(*_sessions[2], 
			start(
				_enable_receive[2], 
				_enable_send[2], 
				_end_receive_packet[2], 
				_end_send_packet[2])).Times(1).After(start_session2);
	
	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);
	_session_controller->start(
		_sessions[1], 
		_enable_receive[1], 
		_enable_send[1], 
		_end_receive_packet[1], 
		_end_send_packet[1]);
	_session_controller->start(
		_sessions[2], 
		_enable_receive[2], 
		_enable_send[2], 
		_end_receive_packet[2], 
		_end_send_packet[2]);

	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(start_session3);
	EXPECT_CALL(*_sessions[1], stop()).Times(1).After(start_session3);
	EXPECT_CALL(*_sessions[2], stop()).Times(1).After(start_session3);
	_session_controller->stop_all();
}

TEST_F(Session_controller_tests, 
when_stop_all_is_called_with_a_session_\
then_all_session_must_be_deleted_from_repo)
{
	Expectation start_session1 = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);
	Expectation start_session2 = 
		EXPECT_CALL(*_sessions[1], 
			start(
				_enable_receive[1], 
				_enable_send[1], 
				_end_receive_packet[1], 
				_end_send_packet[1])).Times(1).After(start_session1);
	Expectation start_session3 = 
		EXPECT_CALL(*_sessions[2], 
			start(
				_enable_receive[2], 
				_enable_send[2], 
				_end_receive_packet[2], 
				_end_send_packet[2])).Times(1).After(start_session2);

	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);
	_session_controller->start(
		_sessions[1], 
		_enable_receive[1], 
		_enable_send[1], 
		_end_receive_packet[1], 
		_end_send_packet[1]);
	_session_controller->start(
		_sessions[2], 
		_enable_receive[2], 
		_enable_send[2], 
		_end_receive_packet[2], 
		_end_send_packet[2]);

	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(start_session3);
	EXPECT_CALL(*_sessions[1], stop()).Times(1).After(start_session3);
	EXPECT_CALL(*_sessions[2], stop()).Times(1).After(start_session3);
	_session_controller->stop_all();

	Expectation start_session4 = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);
	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);

	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(start_session4);
	_session_controller->stop_all();
}

TEST_F(Session_controller_tests, 
when_a_session_is_stopped_then_it_must_be_deleted_from_repo)
{
	Expectation start_session1 = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);
	Expectation start_session2 = 
		EXPECT_CALL(*_sessions[1], 
			start(
				_enable_receive[1], 
				_enable_send[1], 
				_end_receive_packet[1], 
				_end_send_packet[1])).Times(1).After(start_session1);
	Expectation start_session3 = 
		EXPECT_CALL(*_sessions[2], 
			start(
				_enable_receive[2], 
				_enable_send[2], 
				_end_receive_packet[2], 
				_end_send_packet[2])).Times(1).After(start_session2);

	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);
	_session_controller->start(
		_sessions[1], 
		_enable_receive[1], 
		_enable_send[1], 
		_end_receive_packet[1], 
		_end_send_packet[1]);
	_session_controller->start(
		_sessions[2], 
		_enable_receive[2], 
		_enable_send[2], 
		_end_receive_packet[2], 
		_end_send_packet[2]);

	auto selected_number = 2;
	Expectation stop_session = 
		EXPECT_CALL(*_sessions[selected_number], stop()).
			Times(1).After(start_session3);
	_session_controller->stop(_sessions[selected_number]);

	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(stop_session);
	EXPECT_CALL(*_sessions[1], stop()).Times(1).After(stop_session);
	_session_controller->stop_all();
}

TEST_F(Session_controller_tests, 
when_a_Session_is_destructed_all_session_must_be_stopped)
{
	Expectation start_session1 = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);
	Expectation start_session2 = 
		EXPECT_CALL(*_sessions[1], 
			start(
				_enable_receive[1], 
				_enable_send[1], 
				_end_receive_packet[1], 
				_end_send_packet[1])).Times(1).After(start_session1);
	Expectation start_session3 = 
		EXPECT_CALL(*_sessions[2], 
			start(
				_enable_receive[2], 
				_enable_send[2], 
				_end_receive_packet[2], 
				_end_send_packet[2])).Times(1).After(start_session2);

	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);
	_session_controller->start(
		_sessions[1], 
		_enable_receive[1], 
		_enable_send[1], 
		_end_receive_packet[1], 
		_end_send_packet[1]);
	_session_controller->start(
		_sessions[2], 
		_enable_receive[2], 
		_enable_send[2], 
		_end_receive_packet[2], 
		_end_send_packet[2]);

	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(start_session3);
	EXPECT_CALL(*_sessions[1], stop()).Times(1).After(start_session3);
	EXPECT_CALL(*_sessions[2], stop()).Times(1).After(start_session3);
	_session_controller.reset();
}

TEST_F(Session_controller_tests, 
when_a_session_is_stopped_which_not_started_with_session_controller_\
then_nothing_must_be_happen)
{
	Expectation start_session1 = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);
	Expectation start_session2 = 
		EXPECT_CALL(*_sessions[1], 
			start(
				_enable_receive[1], 
				_enable_send[1], 
				_end_receive_packet[1], 
				_end_send_packet[1])).Times(1).After(start_session1);
	Expectation start_session3 = 
		EXPECT_CALL(*_sessions[2], 
			start(
				_enable_receive[2], 
				_enable_send[2], 
				_end_receive_packet[2], 
				_end_send_packet[2])).Times(1).After(start_session2);

	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);
	_session_controller->start(
		_sessions[1], 
		_enable_receive[1], 
		_enable_send[1], 
		_end_receive_packet[1], 
		_end_send_packet[1]);
	_session_controller->start(
		_sessions[2], 
		_enable_receive[2], 
		_enable_send[2], 
		_end_receive_packet[2], 
		_end_send_packet[2]);

	auto session = std::make_shared<Mock_session>();
	_session_controller->stop(session);

	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(start_session3);
	EXPECT_CALL(*_sessions[1], stop()).Times(1).After(start_session3);
	EXPECT_CALL(*_sessions[2], stop()).Times(1).After(start_session3);
	_session_controller->stop_all();
}

TEST_F(Session_controller_tests, 
when_a_session_is_started_twice_then_nothing_must_be_happen)
{
	Expectation start_session1 = 
		EXPECT_CALL(*_sessions[0], 
			start(
				_enable_receive[0], 
				_enable_send[0], 
				_end_receive_packet[0], 
				_end_send_packet[0])).Times(1);

	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);
	_session_controller->start(
		_sessions[0], 
		_enable_receive[0], 
		_enable_send[0], 
		_end_receive_packet[0], 
		_end_send_packet[0]);

	EXPECT_CALL(*_sessions[0], stop()).Times(1).After(start_session1);
}