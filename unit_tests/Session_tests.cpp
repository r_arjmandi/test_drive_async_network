#include "pch.h"

#include "network/Session.h"

#include "test/Mock_socket.h"
#include "test/Mock_circular_buffer.h"
#include "test/Mock_error_code.h"
#include "test/Mock_task_runner.h"
#include "test/Mock_task_handler.h"
#include "test/Mock_IO_executor.h"
#include "test/Mock_session_controller.h"
#include "test/Random_generator.h"

using namespace testing;

class Session_tests : public Test {

public:

	void SetUp() override
	{
		_end_packet = _random_generator.generate_packet();
		_session = std::make_shared<Session>(
			_session_controller,
			_socket,
			_receive_buffer,
			_send_buffer);
	}

	void TearDown() override
	{
		EXPECT_CALL(*_socket, close()).Times(1);
	}

	std::shared_ptr<Session> _session;

	std::shared_ptr<Mock_session_controller> _session_controller {
		std::make_shared<Mock_session_controller>()};
	std::shared_ptr<Mock_circular_buffer<Packet>> _receive_buffer {
		std::make_shared<Mock_circular_buffer<Packet>>()};
	std::shared_ptr<Mock_circular_buffer<Packet>> _send_buffer {
		std::make_shared<Mock_circular_buffer<Packet>>()};
	std::shared_ptr<Mock_socket> _socket {std::make_shared<Mock_socket>()};
	Mock_IO_executor _IO_executor;
	Mock_error_code _error_code;
	Random_generator _random_generator;
	Packet _send_packet;
	Packet _receive_packet;
	Packet _end_packet;

};

TEST_F(Session_tests,
when_start_is_called_just_with_receive_enabled_\
an_async_read_must_be_initiated)
{
	EXPECT_CALL(*_socket, async_read(_, _)).Times(1);

	_session->start(true, false, _end_packet, _end_packet);
}

TEST_F(Session_tests,
when_start_is_called_just_with_send_enabled_\
an_async_write_must_be_initiated)
{
	Expectation check_send_buffer_is_empty = 
		EXPECT_CALL(*_send_buffer, empty()).WillOnce(Return(true));
	EXPECT_CALL(*_socket, get_executor()).After(check_send_buffer_is_empty).
		WillOnce(ReturnRef(_IO_executor));
	EXPECT_CALL(_IO_executor, post(_)).Times(1);

	_session->start(false, true, _end_packet, _end_packet);
}

TEST_F(Session_tests,
when_start_is_called_\
if_send_buffer_is_empty_\
then_checking_emptiness_of_send_buffer_must_be_postponed_\
if_send_buffer_is_not_empty_then_an_async_write_must_be_initiated)
{
	Expectation check_send_buffer_is_empty1 = 
		EXPECT_CALL(*_send_buffer, empty()).WillOnce(Return(true));
	Expectation get_executor1 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty1).
				WillOnce(ReturnRef(_IO_executor));
	std::function<void()> captured_function;
	Expectation post = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor1).
			WillOnce(SaveArg<0>(&captured_function));

	_session->start(false, true, _end_packet, _end_packet);

	Expectation check_send_buffer_is_empty2 = 
		EXPECT_CALL(*_send_buffer, empty()).After(post).
			WillOnce(Return(false));
	Expectation back = EXPECT_CALL(*_send_buffer, back()).
		After(check_send_buffer_is_empty2).WillOnce(ReturnRef(_send_packet));
	EXPECT_CALL(*_socket, async_write(Eq(_send_packet), _)).Times(1).
		After(back);
	captured_function();
}

TEST_F(Session_tests,
when_start_is_called_\
then_must_a_chain_of_async_write_is_initiated)
{
	Expectation check_send_buffer_is_empty1 = 
		EXPECT_CALL(*_send_buffer, empty()).WillOnce(Return(true));
	Expectation get_executor1 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty1).
				WillOnce(ReturnRef(_IO_executor));
	std::function<void()> postponed_function;
	Expectation post = 
		EXPECT_CALL(_IO_executor, post(_)).
			After(get_executor1).WillOnce(SaveArg<0>(&postponed_function));

	_session->start(false, true, _end_packet, _end_packet);

	Expectation check_send_buffer_is_empty2 = 
		EXPECT_CALL(*_send_buffer, empty()).After(post).
			WillOnce(Return(false));
	Expectation back = 
		EXPECT_CALL(*_send_buffer, back()).After(check_send_buffer_is_empty2).
			WillOnce(ReturnRef(_send_packet));
	I_socket::On_async_write_function_type async_write_handler;
	Expectation intiate_async_write = 
		EXPECT_CALL(*_socket, async_write(Eq(_send_packet), _)).After(back).
			WillOnce(SaveArg<1>(&async_write_handler));
	postponed_function();

	Expectation check_err = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_write).
			WillOnce(Return(false));
	Expectation pop_back = 
		EXPECT_CALL(*_send_buffer, pop_back()).Times(1).After(check_err);
	Expectation check_send_buffer_is_empty3 = 
		EXPECT_CALL(*_send_buffer, empty()).After(pop_back).
			WillOnce(Return(true));
	Expectation get_executor2 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty3).
				WillOnce(ReturnRef(_IO_executor));
	std::function<void()> captured_function2;
	Expectation post2 = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor2).
			WillOnce(SaveArg<0>(&captured_function2));
	async_write_handler(_error_code);
}

TEST_F(Session_tests,
when_start_is_called_\
then_must_a_chain_of_async_write_is_initiated_\
until_achieved_packet_equal_to_end_packet)
{
	Expectation check_send_buffer_is_empty1 = 
		EXPECT_CALL(*_send_buffer, empty()).WillOnce(Return(true));
	Expectation get_executor1 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty1).
				WillOnce(ReturnRef(_IO_executor));
	std::function<void()> postponed_function;
	Expectation post = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor1).
			WillOnce(SaveArg<0>(&postponed_function));

	_session->start(false, true, _end_packet, _end_packet);

	Expectation check_send_buffer_is_empty2 = 
		EXPECT_CALL(*_send_buffer, empty()).After(post).
			WillOnce(Return(false));
	Expectation back1 = 
		EXPECT_CALL(*_send_buffer, back()).After(check_send_buffer_is_empty2).
			WillOnce(ReturnRef(_send_packet));
	I_socket::On_async_write_function_type async_write_handler1;
	Expectation intiate_async_write1 = 
		EXPECT_CALL(*_socket, async_write(Eq(_send_packet), _)).After(back1).
			WillOnce(SaveArg<1>(&async_write_handler1));
	postponed_function();

	Expectation check_err = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_write1).
			WillOnce(Return(false));
	Expectation pop_back = 
		EXPECT_CALL(*_send_buffer, pop_back()).Times(1).After(check_err);
	Expectation check_send_buffer_is_empty3 = 
		EXPECT_CALL(*_send_buffer, empty()).After(pop_back).
			WillOnce(Return(false));
	Expectation back2 = 
		EXPECT_CALL(*_send_buffer, back()).After(check_send_buffer_is_empty3).
			WillOnce(ReturnRef(_end_packet));
	I_socket::On_async_write_function_type async_write_handler2;
	Expectation intiate_async_write2 = 
		EXPECT_CALL(*_socket, async_write(Eq(_end_packet), _)).After(back2).
			WillOnce(SaveArg<1>(&async_write_handler2));
	async_write_handler1(_error_code);

	Expectation check_err2 = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_write2).
			WillOnce(Return(false));
	EXPECT_CALL(*_send_buffer, pop_back()).Times(1).After(check_err2);
	async_write_handler2(_error_code);

	Mock::VerifyAndClearExpectations(_session_controller.get());
}

TEST_F(Session_tests,
given_start_is_called_a_chain_of_async_write_is_initiated_\
when_an_error_is_occurred_\
then_chain_must_be_broken)
{
	Expectation check_send_buffer_is_empty1 = 
		EXPECT_CALL(*_send_buffer, empty()).WillOnce(Return(true));
	Expectation get_executor1 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty1).
				WillOnce(ReturnRef(_IO_executor));
	std::function<void()> captured_function1;
	Expectation post = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor1).
			WillOnce(SaveArg<0>(&captured_function1));

	_session->start(false, true, _end_packet, _end_packet);

	Expectation check_send_buffer_is_empty2 = 
		EXPECT_CALL(*_send_buffer, empty()).After(post).
			WillOnce(Return(false));
	Expectation back = 
		EXPECT_CALL(*_send_buffer, back()).After(check_send_buffer_is_empty2).
			WillOnce(ReturnRef(_send_packet));
	I_socket::On_async_write_function_type async_write_handler;
	Expectation intiate_async_write = 
		EXPECT_CALL(*_socket, async_write(Eq(_send_packet), _)).After(back).
			WillOnce(SaveArg<1>(&async_write_handler));
	captured_function1();

	Expectation check_err = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_write).
			WillOnce(Return(true));
	EXPECT_CALL(*_session_controller, stop(Eq(_session))).Times(1).
		After(check_err);
	async_write_handler(_error_code);

	Mock::VerifyAndClearExpectations(_session_controller.get());
}

TEST_F(Session_tests, 
given_a_session_is_constructed_\
when_it_is_destructed_\
then_socket_must_be_stopped_through_session_controller)
{
}

TEST_F(Session_tests, 
when_start_is_called_twice_\
then_nothing_must_be_happen)
{
	EXPECT_CALL(*_socket, async_read(_, _)).Times(1);
	Expectation check_send_buffer_is_empty1 = 
		EXPECT_CALL(*_send_buffer, empty()).WillOnce(Return(true));
	Expectation get_executor1 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty1).
				WillOnce(ReturnRef(_IO_executor));
	Expectation post = 
		EXPECT_CALL(_IO_executor, post(_)).Times(1).After(get_executor1);

	_session->start(true, true, _end_packet, _end_packet);
	_session->start(true, true, _end_packet, _end_packet);
}

TEST_F(Session_tests,
when_start_is_called_twice_\
then_end_send_packet_must_be_unchanged)
{
	Expectation check_send_buffer_is_empty1 = 
		EXPECT_CALL(*_send_buffer, empty()).WillOnce(Return(true));
	Expectation get_executor1 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty1).
				WillOnce(ReturnRef(_IO_executor));
	std::function<void()> postponed_function;
	Expectation post = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor1).
			WillOnce(SaveArg<0>(&postponed_function));

	_session->start(false, true, _end_packet, _end_packet);

	auto otherEndPacket {_random_generator.generate_packet()};
	_session->start(false, true, _end_packet, otherEndPacket);

	Expectation check_send_buffer_is_empty2 = 
		EXPECT_CALL(*_send_buffer, empty()).After(post).
			WillOnce(Return(false));
	Expectation back1 = 
		EXPECT_CALL(*_send_buffer, back()).After(check_send_buffer_is_empty2).
			WillOnce(ReturnRef(_send_packet));
	I_socket::On_async_write_function_type async_write_handler1;
	Expectation intiate_async_write1 = 
		EXPECT_CALL(*_socket, async_write(Eq(_send_packet), _)).After(back1).
			WillOnce(SaveArg<1>(&async_write_handler1));
	postponed_function();

	Expectation check_err = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_write1).
			WillOnce(Return(false));
	Expectation pop_back = 
		EXPECT_CALL(*_send_buffer, pop_back()).Times(1).After(check_err);
	Expectation check_send_buffer_is_empty3 = 
		EXPECT_CALL(*_send_buffer, empty()).After(pop_back).
			WillOnce(Return(false));
	Expectation back2 = 
		EXPECT_CALL(*_send_buffer, back()).After(check_send_buffer_is_empty3).
			WillOnce(ReturnRef(_end_packet));
	I_socket::On_async_write_function_type async_write_handler2;
	Expectation intiate_async_write2 = 
		EXPECT_CALL(*_socket, async_write(Eq(_end_packet), _)).After(back2).
			WillOnce(SaveArg<1>(&async_write_handler2));;
	async_write_handler1(_error_code);

	Expectation check_err2 = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_write2).
			WillOnce(Return(false));
	EXPECT_CALL(*_send_buffer, pop_back()).Times(1).After(check_err2);
	async_write_handler2(_error_code);

	Mock::VerifyAndClearExpectations(_session_controller.get());
}

TEST_F(Session_tests, 
when_start_is_called_and_read_handler_is_called_\
and_pushing_to_buffer_isSuccessful_\
then_a_chian_of_async_read_must_be_called)
{
	I_socket::On_async_write_function_type on_read_handler1;
	Packet captured_packet1;
	EXPECT_CALL(*_socket, async_read(_, _)).
		WillOnce(
			DoAll(SaveArg<0>(&captured_packet1), 
				SaveArg<1>(&on_read_handler1)));
	_session->start(true, false, _end_packet, _end_packet);

	Expectation check_error1 = 
		EXPECT_CALL(_error_code, bool_operator()).WillOnce(Return(false));
	Expectation check_is_full1 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error1).
			WillOnce(Return(false));
	Expectation push_front1 = 
		EXPECT_CALL(*_receive_buffer, push_front(Eq(ByRef(captured_packet1)))).
			Times(1).After(check_is_full1);
	I_socket::On_async_write_function_type on_read_handler2;
	Packet captured_packet2;
	Expectation intiate_async_read1 = 
		EXPECT_CALL(*_socket, async_read(_, _)).After(push_front1).
			WillOnce(
				DoAll(SaveArg<0>(&captured_packet2), 
					SaveArg<1>(&on_read_handler2)));
	on_read_handler1(_error_code);

	Expectation check_error2 = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_read1).
			WillOnce(Return(false));
	Expectation check_is_full2 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error2).
			WillOnce(Return(false));
	Expectation push_front2 = 
		EXPECT_CALL(*_receive_buffer, push_front(Eq(ByRef(captured_packet1)))).
			Times(1).After(check_is_full2);
	I_socket::On_async_write_function_type on_read_handler3;
	EXPECT_CALL(*_socket, async_read(_, _)).After(push_front2).
		After(push_front2).WillOnce(SaveArg<1>(&on_read_handler3));
	on_read_handler2(_error_code);

	Expectation check_error3 = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_read1).
			WillOnce(Return(true));
	EXPECT_CALL(*_session_controller, stop(Eq(_session))).Times(1).
		After(check_error3);
	on_read_handler3(_error_code);

	Mock::VerifyAndClearExpectations(_session_controller.get());
}

TEST_F(Session_tests, 
when_start_is_called_\
then_a_chian_of_async_read_must_be_called_\
until_received_packet_is_equal_to_end_packet)
{
	I_socket::On_async_write_function_type on_read_handler1;
	Packet captured_packet1;
	EXPECT_CALL(*_socket, async_read(_, _)).
		WillOnce(
			DoAll(SaveArg<0>(&captured_packet1), 
				SaveArg<1>(&on_read_handler1)));
	_session->start(true, false, _end_packet, _end_packet);

	Expectation check_error1 = 
		EXPECT_CALL(_error_code, bool_operator()).WillOnce(Return(false));
	Expectation check_is_full1 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error1).
			WillOnce(Return(false));
	Expectation push_front1 = 
		EXPECT_CALL(*_receive_buffer, push_front(Eq(ByRef(captured_packet1)))).
			Times(1).After(check_is_full1);
	I_socket::On_async_write_function_type on_read_handler2;
	Packet captured_packet2;
	Expectation intiate_async_read1 = 
		EXPECT_CALL(*_socket, async_read(_, _)).After(push_front1).
			WillOnce(
				DoAll(SaveArg<0>(&captured_packet2), 
					SaveArg<1>(&on_read_handler2)));
	on_read_handler1(_error_code);

	Expectation check_error2 = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_read1).
			WillOnce(Return(false));
	Expectation check_is_full2 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error2).
			WillOnce(Return(false));
	Expectation push_front2 = 
		EXPECT_CALL(*_receive_buffer, push_front(Eq(ByRef(captured_packet2)))).
			Times(1).After(check_is_full2);
	I_socket::On_async_write_function_type on_read_handler3;
	EXPECT_CALL(*_socket, async_read(_, _)).After(push_front2).
		After(push_front2).WillOnce(
			DoAll(SetArgReferee<0>(_end_packet), 
				SaveArg<1>(&on_read_handler3)));
	on_read_handler2(_error_code);

	Expectation check_error3 = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_read1).
			WillOnce(Return(false));
	Expectation check_is_full3 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error3).
			WillOnce(Return(false));
	Expectation push_front3 = 
		EXPECT_CALL(*_receive_buffer, push_front(Eq(_end_packet))).Times(1).
			After(check_is_full3);
	on_read_handler3(_error_code);

	Mock::VerifyAndClearExpectations(_session_controller.get());
}

TEST_F(Session_tests, 
when_start_is_called_twice_\
then_end_receive_packet_must_be_unchanged)
{
	I_socket::On_async_write_function_type on_read_handler1;
	Packet captured_packet1;
	EXPECT_CALL(*_socket, async_read(_, _)).
		WillOnce(
			DoAll(SaveArg<0>(&captured_packet1),
				SaveArg<1>(&on_read_handler1)));
	_session->start(true, false, _end_packet, _end_packet);
	
	auto otherEndPacket = _random_generator.generate_packet();
	_session->start(true, false, otherEndPacket, _end_packet);

	Expectation check_error1 = 
		EXPECT_CALL(_error_code, bool_operator()).WillOnce(Return(false));
	Expectation check_is_full1 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error1).
			WillOnce(Return(false));
	Expectation push_front1 = 
		EXPECT_CALL(*_receive_buffer, push_front(Eq(ByRef(captured_packet1)))).
			Times(1).After(check_is_full1);
	I_socket::On_async_write_function_type on_read_handler2;
	Packet captured_packet2;
	Expectation intiate_async_read1 = 
		EXPECT_CALL(*_socket, async_read(_, _)).After(push_front1).
			WillOnce(
				DoAll(SaveArg<0>(&captured_packet2), 
					SaveArg<1>(&on_read_handler2)));
	on_read_handler1(_error_code);

	Expectation check_error2 = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_read1).
			WillOnce(Return(false));
	Expectation check_is_full2 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error2).
			WillOnce(Return(false));
	Expectation push_front2 = 
		EXPECT_CALL(*_receive_buffer, push_front(Eq(ByRef(captured_packet2)))).
			Times(1).After(check_is_full2);
	I_socket::On_async_write_function_type on_read_handler3;
	EXPECT_CALL(*_socket, async_read(_, _)).After(push_front2).
		After(push_front2).
			WillOnce(
				DoAll(SetArgReferee<0>(_end_packet), 
					SaveArg<1>(&on_read_handler3)));
	on_read_handler2(_error_code);

	Expectation check_error3 = 
		EXPECT_CALL(_error_code, bool_operator()).After(intiate_async_read1).
			WillOnce(Return(false));
	Expectation check_is_full3 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error3).
			WillOnce(Return(false));
	Expectation push_front3 = 
		EXPECT_CALL(*_receive_buffer, push_front(Eq(_end_packet))).Times(1).
			After(check_is_full3);
	on_read_handler3(_error_code);

	Mock::VerifyAndClearExpectations(_session_controller.get());
}

TEST_F(Session_tests,
given_start_is_called_and_read_handler_is_called_\
when_receive_buffer_is_full_\
then_pushing_to_buffer_must_be_postpone)
{
	I_socket::On_async_write_function_type on_read_handler1;
	Packet captured_packet1;
	Expectation initiate_async_read = 
		EXPECT_CALL(*_socket, async_read(_, _)).
			WillOnce(
				DoAll(SaveArg<0>(&captured_packet1), 
					SaveArg<1>(&on_read_handler1)));
	_session->start(true, false, _end_packet, _end_packet);

	Expectation check_error1 = 
		EXPECT_CALL(_error_code, bool_operator()).After(initiate_async_read).
			WillOnce(Return(false));
	Expectation check_is_full1 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error1).
			WillOnce(Return(true));
	Expectation get_executor1 = 
		EXPECT_CALL(*_socket, get_executor()).After(check_is_full1).
			WillOnce(ReturnRef(_IO_executor));
	std::function<void()> captured_function1;
	Expectation post2 = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor1).
			WillOnce(SaveArg<0>(&captured_function1));
	on_read_handler1(_error_code);

	Expectation check_is_full2 = 
		EXPECT_CALL(*_receive_buffer, full()).After(post2).
			WillOnce(Return(true));
	Expectation get_executor2 = 
		EXPECT_CALL(*_socket, get_executor()).After(check_is_full2).
			WillOnce(ReturnRef(_IO_executor));
	std::function<void()> captured_function2;
	Expectation post3 = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor2).
			WillOnce(SaveArg<0>(&captured_function2));
	captured_function1();

	Expectation check_is_full3 = 
		EXPECT_CALL(*_receive_buffer, full()).After(post3).
			WillOnce(Return(false));
	Expectation push_front = 
		EXPECT_CALL(*_receive_buffer, 
			push_front(Eq(ByRef(captured_packet1)))).Times(1).
				After(check_is_full3);
	I_socket::On_async_write_function_type on_read_handler2;
	Packet captured_packet2;
	Expectation initiate_async_read2 = 
		EXPECT_CALL(*_socket, async_read(_, _)).After(push_front).
			WillOnce(
				DoAll(SaveArg<0>(&captured_packet2), 
					SaveArg<1>(&on_read_handler2)));
	captured_function2();

	Expectation check_error2 = 
		EXPECT_CALL(_error_code, bool_operator()).After(initiate_async_read2).
			WillOnce(Return(false));
	Expectation check_is_full4 = 
		EXPECT_CALL(*_receive_buffer, full()).After(check_error2).
			WillOnce(Return(false));
	Expectation push_front2 = 
		EXPECT_CALL(*_receive_buffer, 
			push_front(Eq(ByRef(captured_packet1)))).Times(1).
				After(check_is_full4);
	EXPECT_CALL(*_socket, async_read(_, _)).After(push_front2).
		After(push_front2);
	on_read_handler2(_error_code);

	Mock::VerifyAndClearExpectations(_session_controller.get());
}

TEST_F(Session_tests,
given_start_is_called_and_send_buffer_is_empty_\
when_stop_is_called_\
then_the_chain_of_async_write_must_be_broken)
{
	Expectation check_send_buffer_is_empty1 = 
		EXPECT_CALL(*_send_buffer, empty()).WillOnce(Return(true));
	Expectation get_executor1 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty1).
				WillOnce(ReturnRef(_IO_executor));
	std::function<void()> captured_function1;
	Expectation post1 = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor1).
			WillOnce(SaveArg<0>(&captured_function1));

	_session->start(false, true, _end_packet, _end_packet);

	Expectation check_send_buffer_is_empty2 = 
		EXPECT_CALL(*_send_buffer, empty()).After(post1).
			WillOnce(Return(true));
	Expectation get_executor2 = 
		EXPECT_CALL(*_socket, get_executor()).
			After(check_send_buffer_is_empty2).
				WillOnce(ReturnRef(_IO_executor));
	std::function<void()> captured_function2;
	Expectation post2 = 
		EXPECT_CALL(_IO_executor, post(_)).After(get_executor2).
			WillOnce(SaveArg<0>(&captured_function2));
	captured_function1();

	_session->stop();

	captured_function2();
}