#include "pch.h"

#include "network/TCP_client.h"

#include "test/Mock_socket.h"
#include "test/Mock_session.h"
#include "test/Mock_IO_context.h"
#include "test/Mock_error_code.h"
#include "test/Mock_socket_factory.h"
#include "test/Mock_circular_buffer.h"
#include "test/Mock_session_factory.h"
#include "test/Mock_IO_context_factory.h"
#include "test/Mock_session_controller.h"
#include "test/Mock_circular_buffer_factory.h"
#include "test/Mock_on_accept_function_object.h"
#include "test/Mock_session_controller_factory.h"
#include "test/Mock_thread_pool_factory.h"
#include "test/Mock_thread_pool_executor.h"
#include "test/Mock_thread_pool.h"
#include "test/Random_generator.h"

using namespace testing;

class TCP_client_tests : public Test {

protected:

	void SetUp() override
	{
		std::fill(_end_receive_packet.begin(), _end_receive_packet.end(), '0');
		std::fill(_end_send_packet.begin(), _end_send_packet.end(), '0');
		_on_connect = [this](const auto& ec, auto se)
		{
			_mock_on_connect(ec, se);
		};
		EXPECT_CALL(*_session_controller_factory, create()).
			WillOnce(Return(_session_controller));
		_tcp_client = std::make_unique<TCP_client>(
			_IO_context,
			_socket_factory,
			_session_factory,
			_session_controller_factory,
			_circular_buffer_factory,
			_send_buffer_size,
			_receive_buffer_size);
	}

	std::unique_ptr<TCP_client> _tcp_client;
	
	const std::string _IP_address {"1.2.3.4"};
	const int _port {123};

	std::shared_ptr<Mock_socket> _socket {std::make_shared<Mock_socket>()};
	std::shared_ptr<Mock_session> _session {std::make_shared<Mock_session>()};
	std::shared_ptr<Mock_IO_context> _IO_context {
		std::make_shared<Mock_IO_context>()};
	std::shared_ptr<Mock_socket_factory> _socket_factory {
		std::make_shared<Mock_socket_factory>()};
	std::shared_ptr<Mock_session_factory> _session_factory {
		std::make_shared<Mock_session_factory>()};
	std::shared_ptr<Mock_session_controller> _session_controller {
		std::make_shared<Mock_session_controller>()};
	std::shared_ptr<Mock_circular_buffer<Packet>> _send_circular_buffer {
		std::make_shared<Mock_circular_buffer<Packet>>()};
	std::shared_ptr<Mock_circular_buffer<Packet>> _receive_circular_buffer {
		std::make_shared<Mock_circular_buffer<Packet>>()};
	std::shared_ptr<Mock_session_controller_factory> 
		_session_controller_factory {
			std::make_shared<Mock_session_controller_factory>()};
	std::shared_ptr<Mock_circular_buffer_factory<Packet>> 
		_circular_buffer_factory {
			std::make_shared<Mock_circular_buffer_factory<Packet>>()};
	Random_generator m_RandomGenerator;
	int _send_buffer_size {m_RandomGenerator.generate_integer(1000, 4000)};
	int _receive_buffer_size {m_RandomGenerator.generate_integer(1000, 4000)};
	bool _enable_receive {m_RandomGenerator.generate_bool()}; 
	bool _enable_send {m_RandomGenerator.generate_bool()}; 
	Packet _end_receive_packet; 
	Packet _end_send_packet;
	I_TCP_client::On_connect_function_type _on_connect;
	Mock_on_accept_function_object _mock_on_connect;
	Mock_error_code _error_code;

};

TEST_F(TCP_client_tests, 
when_async_connect_is_called_\
then_it_must_create_a_socket_and_call_the_async_connect_of_it)
{
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_socket));
	EXPECT_CALL(*_socket, async_connect(_IP_address, _port, _)).Times(1);

	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		[](const auto&, auto) {}, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);
}

TEST_F(TCP_client_tests, 
when_IO_context_runner_is_not_run_\
then_IO_context_runner_stop_function_must_not_be_run)
{
}

TEST_F(TCP_client_tests, 
when_IO_context_runner_is_called_twice_then_nothing_must_be_happend)
{
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_socket));
	EXPECT_CALL(*_socket, async_connect(_IP_address, _port, _)).Times(1);

	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		[](const auto&, auto) {},
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);
	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		[](const auto&, auto) {},
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);
}

TEST_F(TCP_client_tests, 
when_async_connect_is_called_\
then_stop_\
then_async_connect_is_called_\
it_must_create_a_socket_and_call_the_async_connect_of_it)
{
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_socket));
	EXPECT_CALL(*_socket, async_connect(_IP_address, _port, _)).Times(1);

	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		[](const auto&, auto) {},
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	_tcp_client->stop();

	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_socket));
	EXPECT_CALL(*_socket, async_connect(_IP_address, _port, _)).Times(1);

	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		[](const auto&, auto) {},
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);
}

TEST_F(TCP_client_tests, 
when_async_connect_is_called_\
then_it_must_create_a_socket_call_the_async_connect_of_it_\
and_finally_if_is_connected_successfully_\
then_it_must_start_a_session)
{
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_socket));
	I_socket::On_connect_function_type captured_on_connect_function;
	Expectation initiate_connect1 = 
		EXPECT_CALL(*_socket, async_connect(_IP_address, _port, _)).
			WillOnce(SaveArg<2>(&captured_on_connect_function));

	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		_on_connect,
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	Expectation check_error = 
		EXPECT_CALL(_error_code, bool_operator()).After(initiate_connect1).
			WillOnce(Return(false));
	EXPECT_CALL(*_circular_buffer_factory, create(_send_buffer_size)).
		After(check_error).WillRepeatedly(Return(_send_circular_buffer));
	EXPECT_CALL(*_circular_buffer_factory, create(_receive_buffer_size)).
		After(check_error).WillRepeatedly(Return(_receive_circular_buffer));
	EXPECT_CALL(*_session_factory, 
		create(
			Eq(_session_controller), 
			Eq(_socket), 
			Eq(_receive_circular_buffer), 
			Eq(_send_circular_buffer))).WillOnce(Return(_session));
	EXPECT_CALL(*_session_controller, start(
		Eq(_session),
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet)).Times(1);
	Expectation on_connect = 
		EXPECT_CALL(_mock_on_connect, parenthese_op(_, Eq(_session))).Times(1);

	captured_on_connect_function(_error_code);

	EXPECT_CALL(*_session_controller, stop_all()).Times(1).After(on_connect);
}

TEST_F(TCP_client_tests, 
when_async_connect_is_called_\
then_it_must_create_a_socket_call_the_async_connect_of_it_\
and_finally_if_is_not_connected_successfully_\
then_it_must_do_nothing)
{
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_socket));
	I_socket::On_connect_function_type captured_on_connect_function;
	Expectation initiate_connect1 = 
		EXPECT_CALL(*_socket, async_connect(_IP_address, _port, _)).
			WillOnce(SaveArg<2>(&captured_on_connect_function));
	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		_on_connect,
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	Expectation check_error = 
		EXPECT_CALL(_error_code, bool_operator()).After(initiate_connect1).
			WillOnce(Return(true));
	captured_on_connect_function(_error_code);
}

TEST_F(TCP_client_tests, 
when_stop_is_called_then_IO_context_runner_must_be_stopped)
{
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_socket));
	EXPECT_CALL(*_socket, async_connect(_IP_address, _port, _)).Times(1);

	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		_on_connect,
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);
	
	_tcp_client->stop();
}

TEST_F(TCP_client_tests, when_stop_is_called_then_session_must_be_stopped)
{
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_socket));
	I_socket::On_connect_function_type captured_on_connect_function;
	Expectation initiate_connect1 = 
		EXPECT_CALL(*_socket, async_connect(_IP_address, _port, _)).
			WillOnce(SaveArg<2>(&captured_on_connect_function));
	_tcp_client->async_connect(
		_IP_address, 
		_port, 
		_on_connect,
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	Expectation check_error = 
		EXPECT_CALL(_error_code, bool_operator()).After(initiate_connect1).
			WillOnce(Return(false));
	EXPECT_CALL(*_circular_buffer_factory, 
		create(_send_buffer_size)).After(check_error).
			WillRepeatedly(Return(_send_circular_buffer));
	EXPECT_CALL(*_circular_buffer_factory, 
		create(_receive_buffer_size)).After(check_error).
			WillRepeatedly(Return(_receive_circular_buffer));
	EXPECT_CALL(*_session_factory, 
		create(
			Eq(_session_controller), 
			Eq(_socket), 
			Eq(_receive_circular_buffer), 
			Eq(_send_circular_buffer))).WillOnce(Return(_session));
	EXPECT_CALL(*_session_controller, start(
		Eq(_session), 
		_enable_receive,
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet)).Times(1);
	Expectation on_connect = 
		EXPECT_CALL(_mock_on_connect, parenthese_op(_, Eq(_session))).Times(1);
	captured_on_connect_function(_error_code);

	EXPECT_CALL(*_session_controller, stop_all()).Times(1).After(on_connect);
	_tcp_client->stop();
}