#include "pch.h"

#include "network/TCP_server.h"

#include "test/Mock_acceptor.h"
#include "test/Mock_error_code.h"
#include "test/Mock_socket.h"
#include "test/Mock_session_factory.h"
#include "test/Mock_session.h"
#include "test/Mock_on_accept_function_object.h"
#include "test/Mock_acceptor_factory.h"
#include "test/Mock_session_controller.h"
#include "test/Mock_circular_buffer_factory.h"
#include "test/Mock_circular_buffer.h"
#include "test/Mock_IO_context_factory.h"
#include "test/Mock_IO_context.h"
#include "test/Mock_socket_factory.h"
#include "test/Mock_session_controller_factory.h"
#include "test/Mock_thread_pool_factory.h"
#include "test/Mock_thread_pool.h"
#include "test/Mock_thread_pool_executor.h"
#include "test/Random_generator.h"

using namespace testing;

class TCP_server_tests : public Test {

protected:

	void SetUp() override
	{
		_on_accept = [this]
		(const auto& error_code, auto session)
		{
			_mock_on_accept(error_code, session);
		};

		EXPECT_CALL(*_session_controller_factory, create()).
			WillOnce(Return(_session_controller));
		_server = std::make_unique<TCP_server>(
			_IO_context,
			_socket_factory,
			_acceptor_factory,
			_session_factory,
			_session_controller_factory,
			_circular_buffer_factory,
			_send_buffer_size_per_client,
			_receive_buffer_size_per_client);
	}

	std::unique_ptr<TCP_server> _server;

	TCP_server::On_accept_function_type _on_accept;
	Mock_on_accept_function_object _mock_on_accept;
	std::shared_ptr<Mock_socket_factory> _socket_factory {
		std::make_shared<Mock_socket_factory>()};
	std::shared_ptr<Mock_IO_context> _IO_context {
		std::make_shared<Mock_IO_context>()};
	std::shared_ptr<Mock_acceptor_factory> _acceptor_factory {
		std::make_shared<Mock_acceptor_factory>()};
	std::array<std::shared_ptr<Mock_socket>, 4> _sockets {
		std::make_shared<Mock_socket>(),
		std::make_shared<Mock_socket>(),
		std::make_shared<Mock_socket>(),
		std::make_shared<Mock_socket>() };
	std::shared_ptr<Mock_circular_buffer_factory<Packet>> 
		_circular_buffer_factory {
			std::make_shared<Mock_circular_buffer_factory<Packet>>()};
	std::shared_ptr<Mock_circular_buffer<Packet>> _send_circular_buffer {
		std::make_shared<Mock_circular_buffer<Packet>>()};
	std::shared_ptr<Mock_circular_buffer<Packet>> _receive_circular_buffer {
		std::make_shared<Mock_circular_buffer<Packet>>()};
	std::shared_ptr<Mock_session_controller> _session_controller {
		std::make_shared<Mock_session_controller>()};
	std::shared_ptr<Mock_session_controller_factory> 
		_session_controller_factory {
			std::make_shared<Mock_session_controller_factory>()};
	std::shared_ptr<Mock_session_factory> _session_factory {
		std::make_shared<Mock_session_factory>()};
	std::shared_ptr<Mock_acceptor> _acceptor {
		std::make_shared<Mock_acceptor>()};
	std::shared_ptr<Mock_session> _session {std::make_shared<Mock_session>()};
	Random_generator _random_generator;
	int _send_buffer_size_per_client {
		_random_generator.generate_integer(1000, 3000)};
	int _receive_buffer_size_per_client {
		_random_generator.generate_integer(1000, 3000)};
	bool _enable_receive {_random_generator.generate_bool()}; 
	bool _enable_send {_random_generator.generate_bool()}; 
	Packet _end_receive_packet; 
	Packet _end_send_packet;
	Mock_error_code _error_code;
	int _port {1234};

};

TEST_F(TCP_server_tests, 
when_start_async_accept_is_called_then_acceptor_must_be_created)
{
	EXPECT_CALL(*_acceptor_factory, create(Eq(_IO_context), _port)).
		WillOnce(Return(_acceptor));
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_sockets[0]));
	Expectation initiate_accept = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[0]), _)).Times(1);
	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	EXPECT_CALL(*_acceptor, close()).Times(1).After(initiate_accept);
}

TEST_F(TCP_server_tests, 
when_start_async_accept_is_called_twice_\
then_server_must_call_nothing_async_accept_of_acceptor_for_second_time)
{
	EXPECT_CALL(*_acceptor_factory, create(Eq(_IO_context), _port)).
		WillOnce(Return(_acceptor));
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_sockets[0]));
	Expectation initiate_accept = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[0]), _)).Times(1);
	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	EXPECT_CALL(*_acceptor, close()).Times(1).After(initiate_accept);
}

TEST_F(TCP_server_tests, 
when_start_async_accept_is_called_\
then_stop_\
then_start_\
server_must_call_async_accept_of_acceptor_and_run_IO_context_runner)
{
	EXPECT_CALL(*_acceptor_factory, create(Eq(_IO_context), _port)).
		WillOnce(Return(_acceptor));
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_sockets[0]));
	Expectation initiate_accept1 = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[0]), _)).Times(1);
	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	Expectation close_acceptor1 = 
		EXPECT_CALL(*_acceptor, close()).Times(1).After(initiate_accept1);
	_server->stop_async_accept();

	EXPECT_CALL(*_acceptor_factory, create(Eq(_IO_context), _port)).
		WillOnce(Return(_acceptor));
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_sockets[0]));
	Expectation initiate_accept2 = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[0]), _)).Times(1);
	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	EXPECT_CALL(*_acceptor, close()).Times(1).After(initiate_accept2);
}

TEST_F(TCP_server_tests, 
when_a_server_is_constructed_and_then_is_destructed_nothing_must_be_happen)
{
}

TEST_F(TCP_server_tests, 
when_start_async_accept_is_called_\
then_server_must_call_chain_of_async_accept_of_acceptor)
{
	EXPECT_CALL(*_acceptor_factory, create(Eq(_IO_context), _port)).
		WillOnce(Return(_acceptor));
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_sockets[0]));
	I_TCP_acceptor::On_accept_function_type first_captured_on_async_function;
	Expectation initiate_accept1 = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[0]), _)).
			WillOnce(SaveArg<1>(&first_captured_on_async_function));

	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	Expectation check_error1 = 
		EXPECT_CALL(_error_code, bool_operator()).After(initiate_accept1).
			WillOnce(Return(false));
	EXPECT_CALL(*_circular_buffer_factory, 
		create(_send_buffer_size_per_client)).After(check_error1).
			WillOnce(Return(_send_circular_buffer));
	EXPECT_CALL(*_circular_buffer_factory, 
		create(_receive_buffer_size_per_client)).After(check_error1).
			WillOnce(Return(_receive_circular_buffer));
	EXPECT_CALL(*_session_factory, 
		create(
			Eq(_session_controller), 
			Eq(_sockets[0]), 
			Eq(_receive_circular_buffer), 
			Eq(_send_circular_buffer))).WillOnce(Return(_session));
	Expectation start_session1 = 
		EXPECT_CALL(*_session_controller, 
			start(
				Eq(_session), 
				_enable_receive, 
				_enable_send, 
				_end_receive_packet, 
				_end_send_packet)).Times(1);
	Expectation create_socket1 = 
		EXPECT_CALL(*_socket_factory, 
			create(Eq(_IO_context))).
				After(start_session1).WillOnce(Return(_sockets[1]));
	I_TCP_acceptor::On_accept_function_type second_captured_on_async_function;
	Expectation initiate_accept2 = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[1]), _)).
			After(start_session1, create_socket1).
				WillOnce(SaveArg<1>(&second_captured_on_async_function));
	EXPECT_CALL(_mock_on_accept, parenthese_op(_, Eq(_session))).Times(1).
		After(initiate_accept2);

	//simulate the first accept event has been occurred
	first_captured_on_async_function(_error_code);

	Expectation check_error2 = 
		EXPECT_CALL(_error_code, bool_operator()).WillOnce(Return(false));
	EXPECT_CALL(*_circular_buffer_factory, 
		create(_send_buffer_size_per_client)).After(check_error2).
			WillOnce(Return(_send_circular_buffer));
	EXPECT_CALL(*_circular_buffer_factory, 
		create(_receive_buffer_size_per_client)).After(check_error2).
			WillOnce(Return(_receive_circular_buffer));
	EXPECT_CALL(*_session_factory, 
		create(
			Eq(_session_controller), 
			Eq(_sockets[1]), 
			Eq(_receive_circular_buffer), 
			Eq(_send_circular_buffer))).WillOnce(Return(_session));
	Expectation start_session2 = 
		EXPECT_CALL(*_session_controller, 
			start(
				Eq(_session), 
				_enable_receive, 
				_enable_send, 
				_end_receive_packet, 
				_end_send_packet)).Times(1);
	Expectation create_socket2 = 
		EXPECT_CALL(*_socket_factory, 
			create(Eq(_IO_context))).After(start_session2).
				WillOnce(Return(_sockets[2]));
	I_TCP_acceptor::On_accept_function_type third_captured_on_async_function;
	Expectation initiate_accept3 = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[2]), _)).
			After(start_session2, create_socket2).
				WillOnce(SaveArg<1>(&third_captured_on_async_function));
	EXPECT_CALL(_mock_on_accept, parenthese_op(_, Eq(_session))).Times(1).
		After(initiate_accept3);

	//simulate the second accept event has been occurred
	second_captured_on_async_function(_error_code);

	Expectation check_error3 = EXPECT_CALL(_error_code, bool_operator()).
		WillOnce(Return(true));
	Expectation create_socket3 = 
		EXPECT_CALL(*_socket_factory, 
			create(Eq(_IO_context))).After(check_error3).
				WillOnce(Return(_sockets[3]));
	Expectation initiate_accept4 = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[3]), _)).Times(1).
			After(create_socket3);
	Expectation on_accept = 
		EXPECT_CALL(_mock_on_accept, parenthese_op(_, Eq(nullptr))).Times(1).
			After(initiate_accept4);

	//simulate the third accept event has been occurred
	third_captured_on_async_function(_error_code);

	EXPECT_CALL(*_acceptor, close()).Times(1).After(on_accept);
	EXPECT_CALL(*_session_controller, stop_all()).Times(1).After(on_accept);
	
	_server.reset();
	Mock::VerifyAndClearExpectations(_IO_context.get());
}

TEST_F(TCP_server_tests, 
given_a_server_has_started_\
when_stop_async_accept_is_called_\
then_server_must_stop_chain_of_call_async_accept_of_acceptor)
{
	EXPECT_CALL(*_acceptor_factory, create(Eq(_IO_context), _port)).
		WillOnce(Return(_acceptor));
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_sockets[0]));
	I_TCP_acceptor::On_accept_function_type first_captured_on_async_function;
	Expectation initiate_accept = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[0]), _)).
			WillOnce(SaveArg<1>(&first_captured_on_async_function));
	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	EXPECT_CALL(*_acceptor, close()).Times(1).After(initiate_accept);
	_server->stop_async_accept();

	//simulate the first accept event has been occurred
	first_captured_on_async_function(_error_code);
}

TEST_F(TCP_server_tests, 
given_a_server_has_started_\
when_stop_is_called_twice_\
then_server_must_call_nothing)
{
	EXPECT_CALL(*_acceptor_factory, create(Eq(_IO_context), _port)).
		WillOnce(Return(_acceptor));
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_sockets[0]));
	Expectation initiate_accept = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[0]), _)).Times(1);
	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	EXPECT_CALL(*_acceptor, close()).Times(1).After(initiate_accept);
	_server->stop_async_accept();

	_server->stop_async_accept();
}

TEST_F(TCP_server_tests, 
given_a_server_has_started_and_then_stopped_\
when_server_is_destructed_\
then_nothing_must_be_happened)
{
	EXPECT_CALL(*_session_controller_factory, create()).
		WillOnce(Return(_session_controller));
	auto server = std::make_unique<TCP_server>(
		_IO_context,
		_socket_factory,
		_acceptor_factory,
		_session_factory,
		_session_controller_factory,
		_circular_buffer_factory,
		_send_buffer_size_per_client,
		_receive_buffer_size_per_client);

	EXPECT_CALL(*_acceptor_factory, create(Eq(_IO_context), _port)).
		WillOnce(Return(_acceptor));
	EXPECT_CALL(*_socket_factory, create(Eq(_IO_context))).
		WillOnce(Return(_sockets[0]));
	Expectation initiate_accept = 
		EXPECT_CALL(*_acceptor, async_accept(Eq(_sockets[0]), _)).Times(1);
	_server->start_async_accept(
		_port, 
		_on_accept, 
		_enable_receive, 
		_enable_send, 
		_end_receive_packet, 
		_end_send_packet);

	EXPECT_CALL(*_acceptor, close()).Times(1).After(initiate_accept);
	_server->stop_async_accept();

	server.reset();
}