// Test.cpp : Defines the entry point for the console application.
//

#include "pch.h"
#include "gtest/gtest.h"


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	const auto out = RUN_ALL_TESTS();
	system("pause");
	return out;
}

